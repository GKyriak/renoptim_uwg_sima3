# constant simulation parameters
TYPE = 'UWG'
LOCATION = 'C'
ZONE = '1A'

MONTH = 1
DAY = 1
NDAY = 365
DTSIM = 300
DTWEATHER = 3600

AUTOSIZE = False
SENSOCC = 100
LATFOCC = 0.3
RADFOCC = 0.2
RADFEQUIP = 0.5
RADFLIGHT = 0.7

h_ubl1 = 1000  # ubl height - day (m)
h_ubl2 = 80  # ubl height - night (m)
h_ref = 150  # inversion height (m)
H_TEMP = 2  # temperature height (m)
H_WIND = 10  # wind height (m)
C_CIRC = 1.2  # circulation coefficient (default = 1.2 per Bruno (2012))
C_EXCH = 1  # exchange coefficient (default = 1; ref Bruno (2014))
MAXDAY = 150  # max day threshold (W/m^2)
MAXNIGHT = 20  # max night threshold (W/m^2)
WINDMIN = 1  # min wind speed (m/s)
H_OBS = 0.1  # rural average obstacle height (m)

VEGSTART = 4
VEGEND = 11
ALBVEG = 0.25
LATGRSS = 0.4
LATTREE = 0.6
DROAD = 0.5  # road pavement thickness (m)
KROAD = 1  # road pavement conductivity (W/m K)
CROAD = 1600000  # road volumetric heat capacity (J/m^3 K)
# SENSANTH = 20

SCHTRAFFIC = [
        [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.7, 0.8, 1.0, 0.8, 0.8, 1.0, 0.7,
         0.7, 0.8, 0.8, 0.8, 1.0, 0.8, 0.5, 0.5, 0.2, 0.2, 0.2],  # Weekday

        [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.5, 0.5, 0.5, 0.5, 0.3, 0.5, 0.5,
         0.5, 0.5, 0.5, 0.3, 0.5, 0.5, 0.3, 0.3, 0.2, 0.2, 0.2],  # Saturday

        [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.3, 0.3, 0.5, 0.3, 0.5, 0.3,
         0.5, 0.3, 0.5, 0.3, 0.5, 0.3, 0.5, 0.2, 0.2, 0.2, 0.2]  # Sunday
                ]


class UwgOject(object):
    """
    An UWG object is defined by the following properties:

        Properties:

        * epw_path -- Full path of the rural .epw file that will be morphed.
        * new_epw_path -- Full path of the file name of the morphed .epw file.
        * refBEM -- Reference BEMDef matrix defined by built type, era, and zone.
        * refSchedule -- Reference SchDef matrix defined by built type, era, and zone.
        * month -- Number (1-12) representing simulation start month.
        * day -- Number (1-31) representing simulation start day.
        * nday -- Number of days to simulate.
        * dtsim -- Simlation time step in seconds.
        * dtweather -- Number for weather data time-step in seconds.
        * autosize -- Boolean to set HVAC autosize.
        * sensocc -- Sensible heat from occupant [W].
        * latfocc -- Latent heat fraction from occupant.
        * radfocc -- Radiant heat fraction from occupant.
        * radfequip -- Radiant heat fraction from equipment.
        * radflight -- Radiant heat fraction from electric light.
        * h_ubl1 -- Daytime urban boundary layer height in meters.
        * h_ubl2 -- Nighttime urban boundary layer height in meters.
        * h_ref -- Inversion height in meters.
        * h_temp -- Temperature height in meters.
        * h_wind -- Wind height in meters.
        * c_circ -- Wind scaling coefficient.
        * c_exch -- Exchange velocity coefficient.
        * maxday -- Maximum heat flux threshold for daytime conditions [W/m2].
        * maxnight -- Maximum heat flux threshold for nighttime conditions [W/m2].
        * windmin -- Minimum wind speed in m/s.
        * h_obs -- Rural average obstacle height in meters.
        * bldheight -- Urban building height in meters.
        * h_mix -- Fraction of HVAC waste heat released to street canyon.
        * blddensity -- Building footprint density as fraction of urban area.
        * vertohor -- Vertical-to-horizontal urban area ratio.
        * charlength -- Urban characteristic length in meters.
        * albroad -- Urban road albedo.
        * droad -- Thickness of urban road pavement thickness in meters.
        * sensanth -- Street level anthropogenic sensible heat [W/m2].
        * zone -- Index representing an ASHRAE climate zone.
        * grasscover -- Fraction of urban ground covered in grass only.
        * treecover -- Fraction of urban ground covered in trees.
        * vegstart -- Month in which vegetation starts to evapotranspire.
        * vegend -- Month in which vegetation stops evapotranspiration.
        * albveg -- Vegetation albedo.
        * rurvegcover -- Fraction of rural ground covered by vegetation.
        * latgrss -- Fraction of latent heat absorbed by urban grass.
        * lattree -- Fraction latent heat absorbed by urban trees.
        * schtraffic -- Schedule of fractional anthropogenic heat load.
        * kroad -- Road pavement conductivity [W/m-K].
        * croad -- Road pavement volumetric heat capacity [J/m^3K].
        * bld -- Matrix of numbers representing fraction of urban building stock.
        * albroof -- Average building roof albedo.
        * vegroof -- Fraction of roof covered in grass/shrubs.
        * glzr -- Building glazing ratio.
        * albwall -- Building albedo.
        * shgc -- Building glazing Solar Heat Gain Coefficient (SHGC).
        * flr_h -- Building floor height in meters.
        * ref_bem_vector -- List of custom BEMDef objects to override the refBEM.
        * ref_sch_vector -- List of custom SchDef objects to override the refSchedule.
    """

    def __init__(self, name, building_height, blddensity, flr_h,
                 ver_to_hor, charlength, paralLength, orthLength, urbArea, perimeter,
                 bld_list, albroof, vegroof, glzr, albwall, shgc,
                 h_ubl1, h_ubl2, h_ref, albroad,
                 sensanth,
                 grasscover, treeCover, rurVegCover,
                 schedules, mybld
                 ):

        self.name = name
        self.type = TYPE
        self.location = LOCATION
        self.bldheight = building_height
        self.blddensity = blddensity
        self.flr_h = flr_h

        self.vertohor = ver_to_hor
        self.charlength = charlength
        self.paralLength = paralLength
        self.orthLength = orthLength
        self.urbArea = urbArea
        self.perimeter = perimeter

        self.bld = bld_list
        self.schtraffic = SCHTRAFFIC
        self.albroof = albroof
        self.vegroof = vegroof
        self.glzr = glzr
        self.albwall = albwall
        self.shgc = shgc

        self.month = MONTH
        self.day = DAY
        self.nday = NDAY
        self.dtsim = DTSIM
        self.dtweather = DTWEATHER

        self.autosize = AUTOSIZE
        self.sensocc = SENSOCC
        self.latfocc = LATFOCC
        self.radfocc = RADFOCC
        self.radfequip = RADFEQUIP
        self.radflight = RADFLIGHT

        self.h_ubl1 = h_ubl1
        self.h_ubl2 = h_ubl2
        self.h_ref = h_ref
        self.h_temp = H_TEMP
        self.h_wind = H_WIND
        self.c_circ = C_CIRC
        self.c_exch = C_EXCH
        self.maxday = MAXDAY
        self.maxnight = MAXNIGHT
        self.windmin = WINDMIN
        self.h_obs = H_OBS

        self.h_mix = 1  # fraction of building HVAC waste heat set to the street canyon [as opposed to the roof]
        self.albroad = albroad
        self.droad = DROAD  # road pavement thickness (m)
        self.kroad = KROAD  # road pavement conductivity (W/m K)
        self.croad = CROAD  # road volumetric heat capacity (J/m^3 K)
        self.sensanth = sensanth
        # non-building sensible heat at street level [aka. heat from cars pedestrians street cooking etc. ] (W/m^2)

        self.zone = ZONE
        self.grasscover = grasscover
        self.treecover = treeCover
        self.vegstart = VEGSTART
        self.vegend = VEGEND
        self.albveg = ALBVEG
        self.latgrss = LATGRSS
        self.lattree = LATTREE
        self.rurvegcover = rurVegCover
        self.ref_sch_vector = schedules
        self.ref_bem_vector = mybld

