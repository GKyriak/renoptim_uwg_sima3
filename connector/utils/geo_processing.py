import geopandas as gpd


def calculate_modified_perimeter(row: gpd.GeoSeries, bld: gpd.GeoDataFrame):
    """
    Calculate the initial and modified weighted perimeter for a building
    after removing adjacent surfaces.

    Parameters:
    - row (Series): A row from the GeoDataFrame representing a building.
    - bld (GeoDataFrame): The GeoDataFrame containing all buildings.

    Returns:
    - initial_w_perimeter (float): The initial weighted perimeter of the building.
    - w_perimeter (float): The modified weighted perimeter of the building.
    """

    # Extract building geometry from the row
    building_geometry = row['geometry']

    # Find shared edges with other bld
    shared_edges = bld['geometry'].intersection(building_geometry)

    # Calculate initial weighted perimeter based on building height
    initial_w_perimeter = building_geometry.length * row.height
    intersected_perimeter = 0

    # Calculate modified perimeter by considering shared edges
    for idx, intersection in enumerate(shared_edges):
        if intersection.geom_type == 'LineString' or intersection.geom_type == 'MultiLineString':
            intersected_height = min(bld.iloc[idx].height, row.height)
            intersected_perimeter += intersection.length * intersected_height
    w_perimeter = initial_w_perimeter - intersected_perimeter

    return w_perimeter
