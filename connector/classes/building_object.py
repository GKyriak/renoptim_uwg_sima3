# constants
INT_HEAT_NGT = 1
INT_HEAT_DAY = 10
INT_HEAT_FRAD = 0.1
INT_HEAT_FLAT = 0.1
INFIL = 0.26
VENT = 0.0004  # verified with @Paron
COP = 5.2
COOLCAP = 40
HEATEFF = 0.7
INIT_TEMP = 293


class Building(object):
    """
    A building in UWG is defined by the following parameters:
        * type (str)
        * floor_height (float)
        * int_heat_night (float)
        * int_heat_day (float)
        * int_heat_frad (float)
        * int_heat_flat (float)
        * infil (float)
        * vent (float)
        * glazing_ratio (float)
        * u_value (float)
        * condtype (str)
        * cop (float)
        * coolcap (float)
        * heateff (float)
        * initial_temp (float)

    """

    def __init__(self, flr_hgt, glazing_ratio, u_value, shgc, condtype):
        self.type = 'Building'
        self.floor_height = flr_hgt  # Floor height in meters.
        self.int_heat_night = INT_HEAT_NGT  # Nighttime internal sensible heat gain [W/m2].
        self.int_heat_day = INT_HEAT_DAY  # Daytime internal sensible heat gain [W/m2].
        self.int_heat_frad = INT_HEAT_FRAD  # Value between 0 and 1 for radiant fraction of internal gains.
        self.int_heat_flat = INT_HEAT_FLAT  # Value between 0 and 1 for latent fraction of internal gains.
        self.infil = INFIL
        self.vent = VENT
        self.glazing_ratio = glazing_ratio
        self.u_value = u_value
        self.shgc = shgc
        self.condtype = condtype
        self.cop = COP
        self.coolcap = COOLCAP
        self.heateff = HEATEFF
        self.initial_temp = INIT_TEMP


class BuildingObject(object):
    """
    A building Object in UWG is defined by the following parameters:
        * type (str)
        * building (dict)
        * wall (dict)
        * mass (dict)
        * roof (dict)
        * bldtype (str)
        * builtera (str)
    """

    def __init__(self, building_dict, wall_dict, mass_dict, roof_dict, clase_id, builtera):
        self.type = 'BEMDef'
        self.building = building_dict
        self.wall = wall_dict
        self.mass = mass_dict
        self.roof = roof_dict
        self.bldtype = clase_id
        self.builtera = builtera
