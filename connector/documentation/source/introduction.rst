Introduction
------------

The *Urban Heat Island (UHI)* effect is a phenomenon in which urban areas are significantly warmer than surrounding rural areas.
This is primarily caused by the built environment,
which traps and absorbs heat from the sun, as well as human activities such as transportation and energy consumption.
The UHI effect has important implications for public health and comfort, as well as energy consumption and air quality.

This Python package provides a tool for generating a new urban meteorological file by leveraging the well known tool
Urban Weather Generator [Bueno2013]_.


The *UWG Renoptim* package seeks to provide an operative tool to generate new urban meteorological files (in offline mode)
to be exploited in the *Renoptim* project. It also includes an automatic procedure for generating the required input data
to execute an UWG simulation for a given simulation grid, as well as a methodology to construct building archetypes based on the DPE classification system
by exploiting BDNB data.

In an extended version, the tool can provide Urban Heat Island related KPI's, such as nighttime
Urban Heat Island Intensity (`Figure:` :ref:`DT`), Cooling degree Hours Increase (`Figure:` :ref:`CDH`) ratio, etc...

.. figure:: ../images/DT.png
   :align: center
   :width: 600
   :name: DT
   :figclass: align-center

   Calculated average nightime summer Urban Heat Island Intensity for the city of Paris.


.. figure:: ../images/CDH.png
   :align: center
   :width: 600
   :name: CDH
   :figclass: align-center

   Calculated Cooling Degree Hours increase ratio for the city of Paris.

The Python package has been conceptualised and developed by:

`G-E Kyriakodis <https://www.researchgate.net/profile/G-E-Kyriakodis>`_

and funded by `Sat4BDNB` and `Renoptim` projects.

`Email us: <georgios.kyriakodis@cstb.fr>`_ if you have any question.


References
^^^^^^^^^^

.. _Bueno2013:
Bueno, B., Norford, L., Hidalgo, J., & Pigeon, G. (2013). The urban weather generator. Journal of Building Performance Simulation, 6(4), 269-281.
