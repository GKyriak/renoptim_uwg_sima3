# UWG RenOptim
Please navigate for the full [documentation](connector/documentation/source/index.rst)

## Test and Deploy
See [installation](connector/documentation/source/installation.rst)

## Name
`UWG Renoptim v.0`

## Description
See [introduction](connector/documentation/source/introduction.rst)

## Installation
See [installation](connector/documentation/source/installation.rst)

## Usage
See [usage](connector/documentation/source/usage.rst)

## Contributing
See [CONTRIBUTE.md](CONTRIBUTE.md)

## Authors 
- [ ] G-E Kyriakodis 
- (https://www.researchgate.net/profile/G-E-Kyriakodis)
- georgios.kyriakodis@cstb.fr

## Acknowledgments
- [Sat4BDNB SCO project](https://www.spaceclimateobservatory.org/fr/sat4bdnb)
- RENOPTIM project

## License
See [LICENCE](LICENSE)
