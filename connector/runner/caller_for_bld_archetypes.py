import json
import os
from tqdm import tqdm
import numpy as np
import geopandas as gpd
from decimal import Decimal
from connector.utils.constants import DPE_BINS
from connector.utils.materials import optic_mat_obj
from connector.uwg_dict_creation.create_dicts import create_representative_building, generate_material_layer_list, \
    generate_material_layer_list_roof
from connector import data_path, fn_bld_archetypes, fn_bld_archetypes_properties
from connector.sql_queries.request_global_buildings import request_bld_dep_level

default_val = {'insulation_type_wall': 'ITI',
               'insul_thickness': 5.,
               'wall_material': 'beton',
               'wall_thickness': 20.,
               'cooling_generator': 'pac air / air',
               'insulation_type_roof': 'ITI',
               'insulation_type_floor': 'ITI',
               'roof_mat': 'beton',
               'inertia': 'Légère'
               }


# TODO: reformat this script

def convert_decimal_to_float(col: str):
    """

    :param col: Column name
    :return: Column values from decimal.Decimal to float
    """
    for value in col:
        if isinstance(value, Decimal):
            return col.astype(float)
    return col


def generate_rprsntive_bld_dep_dpe(clase_id: str, gdf: gpd.GeoDataFrame, **kwargs):
    """

    :param clase_id:
    :param gdf:
    :param kwargs:
    :return:
    """

    dflt_uval_range = 0.1
    # mean_w_fh, mean_w_gl_rt, mean_w_hgt, mean_w_shgc, mean_w_sur, mean_w_uroof, mean_w_uvw = [np.nan] * 7

    total_nb_bld = len(gdf)
    # DPE Filtering & copy creation of the init gdf
    gdf_f1 = gdf[gdf['classe_bilan_dpe'] == clase_id].copy()

    if len(gdf_f1) == 0:
        return None, None

    gdf.loc[:, 'fl_height'] = gdf['hauteur'] / gdf['nombre_niveau_logement']
    # 2nd selection criterion: Uvalue
    gdf_f1.loc[:, 'ratio'] = gdf_f1['surface'] / gdf_f1['surface'].sum()
    gdf_f1.loc[:, 'fl_height'] = gdf_f1['hauteur'] / gdf_f1['nombre_niveau_logement']  # this thing doesn't work

    gdf_f1.loc[:, 'w_uv'] = gdf_f1['u_mur_exterieur'] * gdf_f1['ratio']
    mean_w_uv = round((gdf_f1['w_uv']).sum(), 2)

    uvalue_range = (mean_w_uv - dflt_uval_range, mean_w_uv + dflt_uval_range)  # Example range for u-value
    filtered_buildings = gdf_f1[(gdf_f1['u_mur_exterieur'].between(*uvalue_range))].copy()

    while len(filtered_buildings) < 0.5 * len(gdf_f1):
        dflt_uval_range += 0.1
        uvalue_range = (mean_w_uv - dflt_uval_range, mean_w_uv + dflt_uval_range)
        filtered_buildings = gdf_f1[(gdf_f1['u_mur_exterieur'].between(*uvalue_range))].copy()

    filtered_buildings.drop('ratio', axis=1, inplace=True)

    mean_w_fh = calc_weighted_params(filtered_buildings, n_cln='w_fh', init_cln='fl_height')
    mean_w_gl_rt = calc_weighted_params(filtered_buildings, n_cln='w_gl_rt', init_cln='pourcentage_surface_baie_vitree_exterieur')
    mean_w_hgt = calc_weighted_params(filtered_buildings, n_cln='w_hgt', init_cln='hauteur')
    mean_w_shgc = calc_weighted_params(filtered_buildings, n_cln='w_shgc', init_cln='facteur_solaire_baie_vitree')
    mean_w_sur = calc_weighted_params(filtered_buildings, n_cln='w_sur', init_cln='surface')
    mean_w_uroof = calc_weighted_params(filtered_buildings, n_cln='w_uvroof', init_cln='u_plancher_haut_deperditif')
    mean_w_uvw = calc_weighted_params(filtered_buildings, n_cln='w_uvw', init_cln='uw')

    variables = [mean_w_fh, mean_w_gl_rt, mean_w_hgt, mean_w_shgc, mean_w_sur, mean_w_uroof, mean_w_uvw]

    if any(np.isnan(var) for var in variables):
        nan_index = next((i for i, x in enumerate(variables) if np.isnan(x)), None)

        if nan_index == 0:
            mean_w_fh = calc_weighted_params(gdf_f1, n_cln='w_fh', init_cln='fl_height')

            if np.isnan(mean_w_fh):
                mean_w_fh = calc_weighted_params(gdf, n_cln='w_fh', init_cln='fl_height')

        elif nan_index == 1:
            mean_w_gl_rt = calc_weighted_params(gdf_f1, n_cln='w_gl_rt', init_cln='pourcentage_surface_baie_vitree_exterieur')

            if np.isnan(mean_w_gl_rt):
                mean_w_gl_rt = calc_weighted_params(gdf, n_cln='w_gl_rt',
                                                    init_cln='pourcentage_surface_baie_vitree_exterieur')
        elif nan_index == 2:
            mean_w_hgt = calc_weighted_params(gdf_f1, n_cln='w_hgt', init_cln='hauteur')

            if np.isnan(mean_w_hgt):
                mean_w_hgt = calc_weighted_params(gdf, n_cln='w_hgt', init_cln='hauteur')

        elif nan_index == 3:
            mean_w_shgc = calc_weighted_params(gdf_f1, n_cln='w_shgc', init_cln='facteur_solaire_baie_vitree')

            if np.isnan(mean_w_shgc):
                mean_w_shgc = calc_weighted_params(gdf, n_cln='w_shgc', init_cln='facteur_solaire_baie_vitree')

        elif nan_index == 4:
            mean_w_sur = calc_weighted_params(gdf_f1, n_cln='w_sur', init_cln='surface')

            if np.isnan(mean_w_sur):
                mean_w_sur = calc_weighted_params(gdf, n_cln='w_sur', init_cln='surface')

        elif nan_index == 5:
            mean_w_uroof = calc_weighted_params(gdf_f1, n_cln='w_uvroof', init_cln='u_plancher_haut_deperditif')

            if np.isnan(mean_w_uroof):
                mean_w_uroof = calc_weighted_params(gdf, n_cln='w_uvroof', init_cln='u_plancher_haut_deperditif')

        elif nan_index == 6:
            mean_w_uvw = calc_weighted_params(gdf_f1, n_cln='w_uvw', init_cln='uw')

            if np.isnan(mean_w_uvw):
                mean_w_uvw = calc_weighted_params(gdf, n_cln='w_uvw', init_cln='uw')
        else:
            raise ValueError('Something went wrong. One of the global dep variables'
                             'is NAN.')
    # variables = [
    #     ('mean_w_fh', 'w_fh', 'fl_height'),
    #     ('mean_w_gl_rt', 'w_gl_rt', 'pourcentage_surface_baie_vitree_exterieur'),
    #     ('mean_w_hgt', 'w_hgt', 'hauteur'),
    #     ('mean_w_shgc', 'w_shgc', 'facteur_solaire_baie_vitree'),
    #     ('mean_w_sur', 'w_sur', 'surface'),
    #     ('mean_w_uroof', 'w_uvroof', 'u_plancher_haut_deperditif'),
    #     ('mean_w_uvw', 'w_uvw', 'uw')
    # ]
    #
    # for variable, n_cln, init_cln in variables:
    #     value = locals().get(variable)
    #     if np.isnan(value):
    #         locals()[variable] = calc_weighted_params(filtered_buildings, n_cln=n_cln, init_cln=init_cln)
    #         value = locals().get(variable)
    #
    #     if isinstance(value, (int, float)) and np.isnan(value):
    #         locals()[variable] = calc_weighted_params(gdf_f1, n_cln=n_cln, init_cln=init_cln)
    #         value = locals().get(variable)
    #
    #         if isinstance(value, (int, float)) and np.isnan(value):
    #             locals()[variable] = calc_weighted_params(gdf, n_cln=n_cln, init_cln=init_cln)
    #
    # if any(isinstance(locals().get(variable), (int, float)) and np.isnan(locals().get(variable)) for variable, _, _ in
    #        variables):
    #     nan_index = next((i for i, (variable, _, _) in enumerate(variables) if
    #                       isinstance(locals().get(variable), (int, float)) and np.isnan(locals().get(variable))), None)
    #
    #     if nan_index is not None:
    #         raise ValueError(f"Something went wrong. Variable {variables[nan_index][0]} is NaN.")

    mean_w_fh = max(min(mean_w_fh, 3.5),2.2)  # unrealistic values - might not be needed after the modification -
    # i let it for the moment

    # extend the calc with BDNB attributes corresponding to conduction
    str_dict = {'insulation_type_wall': 'type_isolation_mur_exterieur',
                'insul_thickness': 'epaisseur_isolation_mur_exterieur_estim',
                'wall_material': 'materiaux_structure_mur_exterieur_simplifie',
                'wall_thickness': 'epaisseur_structure_mur_exterieur',
                'cooling_generator': 'type_generateur_climatisation',
                'insulation_type_roof': 'type_isolation_plancher_haut',
                'insulation_type_floor': 'type_isolation_plancher_bas',
                'roof_mat': 'materiaux_toiture_simplifie',
                'inertia': 'classe_inertie'
                }

    wall_dict = {key: df_string_description_to_unique_filtering(filtered_buildings, val, key) for key, val in
                 str_dict.items()}

    builtera = find_builtera(filtered_buildings, 'annee_construction_dpe')
    condtype = find_cooling_generator(wall_dict['cooling_generator'])

    # generate material lists
    layer_thickness_lst_wall, material_lst_wall = generate_material_layer_list(mean_w_uv, wall_dict, elem_type='wall')
    alb_wall, emis_wall = optic_mat_obj[wall_dict['wall_material']]['albedo'], \
                          optic_mat_obj[wall_dict['wall_material']]['emissivity']

    layer_thickness_lst_roof, material_lst_roof = generate_material_layer_list_roof(mean_w_uroof, wall_dict,
                                                                                    elem_type='roof')
    alb_roof, emis_roof = optic_mat_obj[wall_dict['roof_mat']]['albedo'], optic_mat_obj[wall_dict['roof_mat']][
        'emissivity']

    layer_thickness_lst_floor, material_lst_floor = generate_material_layer_list(mean_w_uv, wall_dict,
                                                                                 elem_type='floor')
    alb_fl, emis_fl = 0.2, 0.9
    vegcoverage = 0.

    # create the representative building as in UWG
    repres_bld = create_representative_building(mean_w_fh, mean_w_gl_rt, mean_w_uvw, mean_w_shgc,
                                                alb_wall, emis_wall, alb_roof, emis_roof, alb_fl, emis_fl,
                                                vegcoverage,
                                                layer_thickness_lst_wall, material_lst_wall,
                                                layer_thickness_lst_roof, material_lst_roof,
                                                layer_thickness_lst_floor, material_lst_floor,
                                                builtera, clase_id, condtype)

    # generate a dict with further properties
    repres_bld_properties = {
        'mean_w_uv': mean_w_uv,
        'mean_w_uvw': mean_w_uvw,
        'mean_w_uroof': mean_w_uroof,
        'mean_w_fh': mean_w_fh,
        'mean_w_gl_rt': mean_w_gl_rt,
        'mean_w_shgc': mean_w_shgc,
        'mean_w_hgt': mean_w_hgt,
        'mean_w_sur': mean_w_sur,
        'nb_buildings': len(filtered_buildings),
        'total_nb_bld': total_nb_bld,
        'rprs_bld_rto': round(len(filtered_buildings) / total_nb_bld, 2) * 100
    }
    repres_bld_properties.update(wall_dict)

    return repres_bld, repres_bld_properties


def calc_weighted_params(gdf: gpd.GeoDataFrame, n_cln: str, init_cln: str):
    """

    :param n_cln:
    :param init_cln:
    :param gdf:
    """
    # Calculation of weighted parameters / DPE level / department

    if 'ratio' in gdf:
        gdf.loc[:, n_cln] = gdf[init_cln] * gdf['ratio']
    else:
        gdf.loc[:, 'ratio'] = gdf['surface'] / gdf['surface'].sum()
        gdf.loc[:, n_cln] = gdf[init_cln] * gdf['ratio']

    wgt_var = gdf[n_cln].sum()

    rnd_wgt_var = round(wgt_var, 2) if not np.isnan(wgt_var) and wgt_var != 0 else np.nan if wgt_var == 0 else wgt_var

    # if not np.isnan(wgt_var) or wgt_var != 0:
    #     rnd_wgt_var = round(wgt_var, 2)
    # else:
    #     if wgt_var == 0:
    #         rnd_wgt_var = np.nan
    #     else:
    #         rnd_wgt_var = wgt_var

    return rnd_wgt_var


def find_builtera(gdf: gpd.GeoDataFrame, column_name: str):
    """

    :param gdf: gpd.GeoDataFrame of building features / DPE class / departement
    :param column_name: (str) BDNB column of : annee_construction_dpe

    :return: (str) UWG builtera param
    """
    try:
        for desc in gdf[column_name].value_counts().index:
            if desc is not None and desc != 'inconnu':
                built_era_dens = int(desc)
                break

        if built_era_dens < 1981:
            builtera = 'pre80'
        elif 1981 < built_era_dens < 2012:
            builtera = 'pst80'
        else:
            builtera = 'new'
    except UnboundLocalError:
        column_name = 'periode_construction_dpe'
        for desc in gdf[column_name].value_counts().index:
            if desc is not None and desc != 'inconnu':
                built_era_dens = desc
                break

        # hardcoded all possible values from BDNB
        pre80_list = ['avant 1948', '1948-1974', '1975-1977', '1978-1982']
        pst80_list = ['1983-1988', '1989-2000', '2001-2005', '2006-2012']
        new_list = ['2013-2021', 'après 2021']
        if built_era_dens in pre80_list:
            builtera = 'pre80'
        elif built_era_dens in pst80_list:
            builtera = 'pst80'
        elif built_era_dens in new_list:
            builtera = 'new'
        else:
            for desc in gdf['classe_bilan_dpe'].value_counts().index:
                if desc == 'A':
                    builtera = 'new'
                elif desc == 'F' or desc == 'G':
                    builtera = 'pre80'
                else:
                    builtera = 'pst80'

    return builtera


def find_cooling_generator(gentype: str):
    """

    :param gentype: (str) description of the cooling system in BDNB

    :return: (str) description of the cooling system in UWG
    """

    # hardcoded all possible values from BDNB
    # generators_list = ['pac air / air', 'pac air / eau', 'pac eau / eau', 'pac gaz',
    #                    'pac geothermique', 'reseau de froid']  # unique values BDNB

    if 'eau' in gentype and 'reseau' not in gentype:
        condtype = 'WATER'
    else:
        condtype = 'AIR'

    return condtype


def df_string_description_to_unique_filtering(gdf: gpd.GeoDataFrame, column_name: str, dict_key: str):
    unique_descriptions = gdf[column_name].unique()
    description_to_integer = {description: i for i, description in enumerate(unique_descriptions)}
    gdf[column_name] = gdf[column_name].map(description_to_integer)

    insulation_type = None
    for desc in gdf[column_name].value_counts().index:
        if unique_descriptions[desc] is not None and unique_descriptions[desc] != 'inconnu' \
                and unique_descriptions[desc] != 'autre':
            if isinstance(unique_descriptions[desc], float):
                if not np.isnan(unique_descriptions[desc]):
                    insulation_type = unique_descriptions[desc]
                else:
                    continue
            elif isinstance(unique_descriptions[desc], str):
                insulation_type = unique_descriptions[desc]
            break

    if insulation_type is None:
        print(f"\n {column_name} not assigned. Default value is attributed.")
        insulation_type = default_val[dict_key]

    if dict_key.__contains__('thickness'):
        return float(insulation_type)

    return insulation_type


def print_message_bld_archetypes():
    message_init = 'Generation of building archetypes'
    print('\n')
    print('#' * (len(message_init) + 5))
    print('# ', message_init, '#')
    print('#' * (len(message_init) + 5))
    print('#' * (len(message_init) + 5))
    print('\n')


def generate_bld_archetypes(DEP_LIST, **kwargs):

    print_message_bld_archetypes()

    to_save = kwargs.get('save_option')

    bld_dic = {}
    bld_dic_prop = {}

    total_iterations = len(DEP_LIST)
    pbar = tqdm(total=total_iterations)

    for dep_id in DEP_LIST:
        pbar.set_description(f"Processing Dep: {dep_id}")
        bld = request_bld_dep_level(dep_id=dep_id)
        # Apply the function to each column
        bld = bld.apply(convert_decimal_to_float)

        bld.loc[:, 'surface'] = bld.area

        bins = DPE_BINS

        bld_dic[dep_id] = {k: {} for k in bins}
        bld_dic_prop[dep_id] = {k: {} for k in bins}

        classed_gdf = bld.copy()
        for clase in bins:

            repr_bld, repr_bld_prop = generate_rprsntive_bld_dep_dpe(clase, classed_gdf)

            bld_dic[dep_id][clase] = repr_bld
            bld_dic_prop[dep_id][clase] = repr_bld_prop

        pbar.update(1)

    if to_save:
        dump_bld_archetypes_dicts(bld_dic, bld_dic_prop)
    else:
        return bld_dic, bld_dic_prop


def dump_bld_archetypes_dicts(bld_dic, bld_dic_prop):
    bld_json = json.dumps(bld_dic)

    with open(os.path.join(data_path, fn_bld_archetypes), "w") as json_file:
        json_file.write(bld_json)

    bld_json_prop = json.dumps(bld_dic_prop)

    with open(os.path.join(data_path, fn_bld_archetypes_properties), "w") as json_file:
        json_file.write(bld_json_prop)

    print('\n File saved \n')
