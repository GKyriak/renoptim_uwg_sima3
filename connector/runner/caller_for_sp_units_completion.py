import json
import os
from tqdm import tqdm
from connector import uwg_param_files_path, data_path, \
    fn_bld_archetypes, fn_bld_archetypes_properties
from connector.classes.uwg_object import UwgOject
from connector.sql_queries.request_spatial_units import param_file_dic, \
    request_bld_data_ver_to_hor, request_bld_data_classification
from connector.utils.fictive_bld_density import generate_fictive_bld_density, check_validity_bld_density
from connector.utils.constants import DPE_BINS
from connector.utils.geo_processing import calculate_modified_perimeter
from connector.utils.schedules import create_uwg_schedules_RT
import numpy as np


def fill_in_spatial_units_params(uwg_sp_units, save_file=True):

    with open(os.path.join(data_path, fn_bld_archetypes), 'r') as file:
        global_data = json.load(file)

    with open(os.path.join(data_path, fn_bld_archetypes_properties), 'r') as file:
        global_data_prop = json.load(file)

    # uwg_sp_units = uwg_sp_units[:5]
    total_iterations = len(uwg_sp_units)
    pbar = tqdm(total=total_iterations)

    for index, row in uwg_sp_units.iterrows():
        pbar.set_description(f"Processing Grid: {index}")

        param_file_dic[row['sp_id']] = {'meteo_fn': row['weather_station_file_name'],
                                        'uwg_param': None,
                                        'simulation': None
                                        }

        dep_id = row['code_iris'][:2]
        bld = request_bld_data_ver_to_hor(row['sp_id'])
        grasscover, treecover, rurvegcover = row['short_ratio'] / 100, row['tall_ratio'] / 100, row['rurvegcover'] / 100

        # 1 case of rural cells
        if bld.empty:
            ins_dic = {
                'ver_to_hor': 0.,
                'bld_dens': 0.,
                'bld_hgt': 0.
                }

            param_file_dic[row['sp_id']]['simulation'] = False
            param_file_dic[row['sp_id']]['uwg_param'] = None

            pbar.update(1)
            continue
        else:
            # 2 case of urban cells
            bld['surface'] = bld.area
            bld['ratio'] = bld.surface / sum(bld.area)
            bld['w_height'] = bld.height * bld.ratio

            bld['w_perimeter'] = bld.apply(lambda row: calculate_modified_perimeter(row, bld), axis=1)

            ver_to_hor = float(round(sum(bld['w_perimeter'].values) / row.surface, 4))

            blddensity = float(round(np.sum(bld.area.values) / row.surface, 4))

            building_height = float(round(sum(bld['w_height'].values), 4))

            ins_dic = {
                'ver_to_hor': ver_to_hor,
                'bld_dens': blddensity,
                'bld_hgt': building_height
            }

        bld_dpe = request_bld_data_classification(row['sp_id'])

        if bld_dpe.empty:
            param_file_dic[row['sp_id']]['simulation'] = False
            param_file_dic[row['sp_id']]['uwg_param'] = None

            pbar.update(1)
            continue

        if blddensity < 0.12 or grasscover + treecover > 0.6:
            param_file_dic[row['sp_id']]['simulation'] = False
            param_file_dic[row['sp_id']]['uwg_param'] = None

            pbar.update(1)
            continue

        filtered_bld = bld_dpe[
            (bld_dpe['type_dpe'].notna()) & (bld_dpe['type_dpe'] == 'dpe arrêté 2021 3cl logement')].copy()

        if filtered_bld.empty:
            uwg_sp_units.at[index, 'dpe_ratio'] = 0

            bld_list = [[k, global_data[dep_id][k]['builtera'], round(v['rprs_bld_rto']/100,2)] for k, v in
                        global_data_prop[dep_id].items() if v['rprs_bld_rto'] != 0]
            total_sum = sum(item[2] for item in bld_list)

            # Generate the new list with normalized values
            new_bld_ratio = [[item[0], item[1], item[2] / total_sum] for item in bld_list]
            ref_bem_vector = [global_data[dep_id][k[0]] for k in bld_list]
            albroof = np.mean([k['roof']['albedo'] for k in ref_bem_vector])
            albwall = np.mean([k['wall']['albedo'] for k in ref_bem_vector])

            flr_h = min(np.mean([k['building']['floor_height'] for k in ref_bem_vector]), 3.5)
            glzr = np.mean([k['building']['glazing_ratio'] for k in ref_bem_vector])
            shgc = np.mean([k['building']['shgc'] for k in ref_bem_vector])

            schedules = [create_uwg_schedules_RT(k['bldtype'], k['builtera']) for k in ref_bem_vector]
            if check_validity_bld_density(blddensity, treecover, grasscover):

                uwg = UwgOject(row['sp_id'], building_height, blddensity, flr_h,
                               ver_to_hor, row['length'], row['width'], row['length'], row['surface'], row['perimeter'],
                               new_bld_ratio, albroof, 0, glzr, albwall, shgc,
                               1000, 80, 150, 0.1, 20,
                               grasscover, treecover, rurvegcover,
                               schedules, ref_bem_vector)
            else:
                nblddensity, ntreecover, ngrasscover = generate_fictive_bld_density(blddensity,
                                                                                    treecover, grasscover)
                uwg = UwgOject(row['sp_id'], building_height, nblddensity, flr_h,
                               ver_to_hor, row['length'], row['width'], row['length'], row['surface'], row['perimeter'],
                               new_bld_ratio, albroof, 0, glzr, albwall, shgc,
                               1000, 80, 150, 0.1, 20,
                               grasscover, ntreecover, ngrasscover,
                               schedules, ref_bem_vector)

            param_dic = uwg.__dict__

            if len(param_dic) < 51:
                raise Exception('Wrong input data for UWG, should have {} elements while it has {}').format(51,
                                                                                                            len(param_dic))
            param_file_dic[row['sp_id']]['uwg_param'] = param_dic
            param_file_dic[row['sp_id']]['simulation'] = True

            pbar.update(1)
            continue

        bins = DPE_BINS

        class_counts = filtered_bld['classe_bilan_dpe'].value_counts(normalize=True,
                                                                     sort=False).reindex(bins,
                                                                                         fill_value=0)
        if (class_counts == 0).all():
            raise Exception('All of the DPE classes present 0 ratio')

        bld_list = []
        ref_bem_vector = []
        for bin_value, percentage in class_counts.items():

            classed_bld = filtered_bld[filtered_bld['classe_bilan_dpe'] == bin_value].copy()
            if classed_bld.empty:
                continue
            # TODO: might cause a problem
            bld_list.append([bin_value, global_data[dep_id][bin_value]['builtera'], percentage])
            ref_bem_vector.append(global_data[dep_id][bin_value])

        albroof = np.mean([k['roof']['albedo'] for k in ref_bem_vector])
        albwall = np.mean([k['wall']['albedo'] for k in ref_bem_vector])

        flr_h = min(np.mean([k['building']['floor_height'] for k in ref_bem_vector]), 3.5)
        glzr = np.mean([k['building']['glazing_ratio'] for k in ref_bem_vector])
        shgc = np.mean([k['building']['shgc'] for k in ref_bem_vector])

        schedules = [create_uwg_schedules_RT(k['bldtype'], k['builtera']) for k in ref_bem_vector]

        if check_validity_bld_density(blddensity, treecover, grasscover):
            pass
        else:
            raise ValueError('Building density must be reduced.')

        uwg = UwgOject(row['sp_id'], building_height, blddensity, flr_h,
                       ver_to_hor, row['length'], row['width'], row['length'], row['surface'], row['perimeter'],
                       bld_list, albroof, 0, glzr, albwall, shgc,
                       1000, 80, 150, 0.1, 20,
                       grasscover, treecover, rurvegcover,
                       schedules, ref_bem_vector)

        param_dic = uwg.__dict__

        if len(param_dic) < 51:
            raise Exception('Wrong input data for UWG, should have {} elements while it has {}'.format(51,
                                                                                                        len(param_dic)))

        param_file_dic[row['sp_id']]['uwg_param'] = param_dic
        param_file_dic[row['sp_id']]['simulation'] = True

        pbar.update(1)
    pbar.close()

    print(' \n Dumping the param file')
    print(' \n Length param dict: ', len(param_file_dic))
    print(' \n Length spatial units: ', total_iterations)

    if len(param_file_dic) != total_iterations:
        raise Exception('Something went wrong, should have {} grids while input data correspond to {} grids'.format(
            total_iterations, len(param_file_dic)))

    if save_file:

        res_path = os.path.join(uwg_param_files_path, '_uwg_dic_new.json')
        param_file_json = json.dumps(param_file_dic)

        with open(res_path, "w") as json_file:
            json_file.write(param_file_json)

    else:
        return param_file_dic
