Vegetation
----------

For the estimation of urban greenery, we employed a combination of high-resolution (10-meter spatial resolution)
multispectral image data derived from the Sentinel-2 mission program.

The data correspond to
`OSO-OCS2021 <https://www.theia-land.fr/product/carte-doccupation-des-sols-de-la-france-metropolitaine/>`_  [Inglada2017]_
and Urban Atlas Street Layer 2018 `(UASL) <https://land.copernicus.eu/local/urban-atlas>`_ products
provided by  `Pôle Theia <https://www.theia-land.fr/>`_ and `COPERNICUS <https://www.copernicus.eu/en>`_ program respectively.

.. figure:: ../images/oso.png
   :align: center
   :width: 800
   :name: oso_png
   :figclass: align-center

   The OCS land cover map for the city of Paris. The legend represents the 23 classification classes.

The OSO land cover map (`Figure:` :ref:`oso_png`) is a national (French) product derived from the collaboration of the `Land Cover SEC` (Scientific Expertise Centre) containing 23 classification types of urban and natural spaces.
The main advantage of the OSO data is the annual update, allowing the detection of changes in land use and cover.

The Urban Atlas Street Layer product (`Figure:` :ref:`uasl_png`), derived from the Urban Atlas monitoring service, is a European database of urban vegetation types.
It includes contiguous rows or patches of trees covering 500m\ :sup:`2` or more and with a minimum width of 10m.

.. figure:: ../images/uasl.png
   :align: center
   :width: 800
   :name: uasl_png
   :figclass: align-center

   The OCS land cover map for the city of Paris. The legend represents the 23 classification classes.

References
^^^^^^^^^^

.. _Inglada2017:
Inglada, J., Vincent, A., Arias, M., Tardy, B., Morin, D., & Rodes, I. (2017). Operational high resolution land cover map production at the country scale using satellite image time series. Remote Sensing, 9(1), 95.
