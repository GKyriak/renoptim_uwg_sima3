Spatial Units
-------------

The spatial units data correspond to a structured grid provided by  `INSEE <https://www.insee.fr/fr/statistiques/4192935?sommaire=4176305>`_ ,
as shown in Figure: :ref:`units_png`


.. figure:: ../images/units.png
   :align: center
   :width: 600
   :name: units_png
   :figclass: align-center

   Input spatial units: Visualization of the calculation grid (represented by red dashed lines) for the city of Paris.
