from setuptools import setup, find_packages
import os
import platform

if platform.system() == 'Windows':
    path_sep = '\\'
else:
    sys = platform.system()
    raise SystemError('The actual system version: {} is not supported.'.format(sys))

root_dir = os.path.dirname(os.path.realpath(__file__))
renoptim_uwg_python_path = os.path.join(root_dir, 'connector')

with open("README.md", "r") as fh:
    long_description = fh.read()

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

my_pkg = find_packages(exclude=['connector.tests',
                                'connector.post_processing'
                                ]
                       )

data_extensions = ['.bui', '.lib', '.yml']
data_files = []
for r, d, f in os.walk(renoptim_uwg_python_path):
    for file in f:
        for ext in data_extensions:
            if ext in file:
                file_path = (os.path.join(r, file).replace(renoptim_uwg_python_path, '').
                             replace(path_sep, '/').strip('/'))
                data_files.append(file_path)


setup(
    name="renoptim_uwg",
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    author="G-E Kyriakodis",
    author_email="georgios.kyriakodis@cstb.fr",
    description=" The renoptim_uwg Python application automates and models the urban heat island effect by utilizing"
                " the Urban Weather Generator package.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/GKyriak/renoptim_uwg",
    packages=my_pkg,
    package_data={'connector': data_files},
    install_requires=requirements,
    extras_require={
        'setuptools': ["setuptools>=40.8.0"],
        'build-backend': ["setuptools.build_meta"],
        'test': ['pytest'],
    },
    classifiers=[
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: Implementation :: CPython",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
)
