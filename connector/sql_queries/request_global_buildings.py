import sqlalchemy
from shapely import wkb
import geopandas as gpd
from connector import HOST, USER, PORT, DBNAME, PASSWORD, ACTIVE_SCHEMA, PG_ROLE
from connector.utils.sql_general_functions import exec_sqlalchemy_query

from connector.sql_queries import table_bat_cstr, table_bat_syn_env, table_bat_dpe

host = HOST
user = USER
port = PORT
dbname = DBNAME
password = PASSWORD
active_schema = ACTIVE_SCHEMA
database_url = f"postgresql://{user}:{password}@{host}:{port}/{dbname}"
engine = sqlalchemy.create_engine(database_url)


def request_bld_dep_level(**kwargs):
    """
    Request to select all building features in a given department

    :param kwargs: (str) of the department id
    :return:
    """

    dep_id = kwargs.get('dep_id')

    q_bld_dep = f"""
    select * from (
    SELECT 
    bc.geom_cstr, bc.batiment_groupe_id, bc.hauteur, bc.code_iris,
    bc.code_departement_insee, 
    bgse.materiaux_toiture_simplifie, bgse.materiaux_structure_mur_exterieur_simplifie,
    type_dpe, bgdr.type_batiment_dpe,
    periode_construction_dpe, annee_construction_dpe,
    nombre_niveau_logement, nombre_niveau_immeuble,
    surface_habitable_immeuble, surface_habitable_logement,
    conso_5_usages_ep_m2, conso_5_usages_ef_m2, emission_ges_5_usages_m2,
    classe_bilan_dpe, classe_emission_ges,
    classe_conso_energie_arrete_2012,
    classe_emission_ges_arrete_2012,
    conso_3_usages_ep_m2_arrete_2012,
    emission_ges_3_usages_ep_m2_arrete_2012,
    type_installation_chauffage,
    type_energie_chauffage,
    type_generateur_chauffage,
    nb_generateur_chauffage,
    nb_installation_chauffage,
    type_energie_climatisation,
    type_generateur_climatisation,
    type_installation_ecs,
    type_energie_ecs,
    type_generateur_ecs,
    type_generateur_ecs_anciennete,
    nb_generateur_ecs,
    nb_installation_ecs,
    type_ventilation,	
    bgdr.type_vitrage,
    bgdr.type_materiaux_menuiserie,
    bgdr.type_gaz_lame,
    bgdr.epaisseur_lame,
    bgdr.surface_vitree_nord,
    bgdr.surface_vitree_sud,
    bgdr.surface_vitree_ouest,
    bgdr.surface_vitree_est,
    bgdr.surface_vitree_horizontal,
    bgdr.u_baie_vitree,
    bgdr.uw,
    bgdr.facteur_solaire_baie_vitree,
    bgdr.pourcentage_surface_baie_vitree_exterieur,
    bgdr.type_isolation_mur_exterieur,
    bgdr.epaisseur_isolation_mur_exterieur_estim,
    bgdr.materiaux_structure_mur_exterieur,
    bgdr.epaisseur_structure_mur_exterieur,
    bgdr.surface_mur_totale,
    bgdr.surface_mur_exterieur,
    bgdr.surface_mur_deperditif,
    bgdr.u_mur_exterieur,
    bgdr.l_local_non_chauffe_mur,
    bgdr.type_isolation_plancher_bas,
    bgdr.type_plancher_bas_deperditif,
    bgdr.surface_plancher_bas_totale,
    bgdr.surface_plancher_bas_deperditif,
    bgdr.u_plancher_bas_final_deperditif,
    bgdr.u_plancher_bas_brut_deperditif,
    bgdr.type_isolation_plancher_haut,	
    bgdr.type_plancher_haut_deperditif,
    bgdr.surface_plancher_haut_totale,
    bgdr.surface_plancher_haut_deperditif,
    bgdr.u_plancher_haut_deperditif,
    bgdr.classe_inertie,	
    bgdr.deperdition_mur,
    bgdr.deperdition_baie_vitree,
    bgdr.deperdition_plancher_bas,
    bgdr.deperdition_plancher_haut,
    bgdr.deperdition_pont_thermique,
    bgdr.deperdition_porte
    FROM 
    {table_bat_cstr} bc
    join 
    {table_bat_syn_env} bgse
    on
    bc.batiment_groupe_id =bgse.batiment_groupe_id
    join
    {table_bat_dpe} bgdr
    ON
    bc.batiment_groupe_id = bgdr.batiment_groupe_id
    WHERE bc.fictive_geom_cstr = false
) bld_data
WHERE
type_dpe like 'dpe arrêté 2021 3cl logement' and code_departement_insee like '{dep_id}'
;
"""

    d = exec_sqlalchemy_query(engine, PG_ROLE, q_bld_dep, type='fetchall')

    col = ['geom_cstr,batiment_groupe_id,hauteur,code_iris,code_departement_insee, materiaux_toiture_simplifie, '
           'materiaux_structure_mur_exterieur_simplifie, type_dpe,type_batiment_dpe,'
           'periode_construction_dpe,annee_construction_dpe,nombre_niveau_logement,nombre_niveau_immeuble,'
           'surface_habitable_immeuble,surface_habitable_logement,conso_5_usages_ep_m2,conso_5_usages_ef_m2,'
           'emission_ges_5_usages_m2,classe_bilan_dpe,classe_emission_ges,classe_conso_energie_arrete_2012,'
           'classe_emission_ges_arrete_2012,conso_3_usages_ep_m2_arrete_2012,emission_ges_3_usages_ep_m2_arrete_2012, '
           'type_installation_chauffage,type_energie_chauffage,type_generateur_chauffage,nb_generateur_chauffage,'
           'nb_installation_chauffage,type_energie_climatisation,type_generateur_climatisation,type_installation_ecs,'
           'type_energie_ecs,type_generateur_ecs,type_generateur_ecs_anciennete,nb_generateur_ecs,'
           'nb_installation_ecs,type_ventilation,type_vitrage,type_materiaux_menuiserie,type_gaz_lame,epaisseur_lame,'
           'surface_vitree_nord,surface_vitree_sud,surface_vitree_ouest,surface_vitree_est,surface_vitree_horizontal,'
           'u_baie_vitree,uw,facteur_solaire_baie_vitree,pourcentage_surface_baie_vitree_exterieur,'
           'type_isolation_mur_exterieur,epaisseur_isolation_mur_exterieur_estim,materiaux_structure_mur_exterieur,'
           'epaisseur_structure_mur_exterieur,surface_mur_totale,surface_mur_exterieur,surface_mur_deperditif,'
           'u_mur_exterieur,l_local_non_chauffe_mur,type_isolation_plancher_bas,type_plancher_bas_deperditif,'
           'surface_plancher_bas_totale,surface_plancher_bas_deperditif,u_plancher_bas_final_deperditif,'
           'u_plancher_bas_brut_deperditif,type_isolation_plancher_haut,type_plancher_haut_deperditif,'
           'surface_plancher_haut_totale,surface_plancher_haut_deperditif,u_plancher_haut_deperditif,classe_inertie,'
           'deperdition_mur,deperdition_baie_vitree,deperdition_plancher_bas,deperdition_plancher_haut,'
           'deperdition_pont_thermique,deperdition_porte']

    columns = [s.strip() for s in col[0].split(',')]
    geom = [wkb.loads(bytes.fromhex(k[0])) for k in d]
    df = gpd.GeoDataFrame(d, columns=columns, geometry=geom, crs='EPSG:2154')

    return df

#
# # kmean clustering
# # gdf[(gdf['u_mur_exterieur'] > 0.24) & (gdf['u_mur_exterieur'] < 0.25)]
# # gdf['materiaux_structure_mur_exterieur'].value_counts()
#
#
# from sklearn.cluster import KMeans
#
# # Assuming you have already prepared your data X = gdf[['materiaux_structure_mur_exterieur', 'u_mur_exterieur',
# 'epaisseur_structure_mur_exterieur']]  # Features to use for clustering
#
# # Choose the number of clusters (K) based on domain knowledge or methods like the elbow method
# k = 3  # Example number of clusters
#
# # Initialize the KMeans model
# kmeans = KMeans(n_clusters=k)
#
# # Fit the model to the data
# kmeans.fit(X)
#
# # Get cluster labels for each data point
# cluster_labels = kmeans.labels_
#
# # Add the cluster labels to your GeoDataFrame
# gdf['cluster_labels'] = cluster_labels
#
# # Convert categorical wall structure data into numerical format using one-hot encoding
# one_hot_encoded = pd.get_dummies(gdf['materiaux_structure_mur_exterieur'], prefix='materiaux_structure_mur_exterieur')
#
# # Concatenate the one-hot encoded data with the original GeoDataFrame
# gdf_encoded = pd.concat([gdf, one_hot_encoded], axis=1)
#
# # Drop the original wall structure column since it's no longer needed
# gdf_encoded.drop(columns=['materiaux_structure_mur_exterieur'], inplace=True)
#
#
# a =gdf[['ratio', 'u_mur_exterieur', 'epaisseur_structure_mur_exterieur']]
#
# a = a.dropna(subset=['ratio', 'u_mur_exterieur', 'epaisseur_structure_mur_exterieur'])
#
# a.cluster_labels.value_counts()
#
# X = a[['ratio', 'u_mur_exterieur', 'epaisseur_structure_mur_exterieur']]  # Features to use for clustering
# # Choose the number of clusters (K) based on domain knowledge or methods like the elbow method
# k = 3  # Example number of clusters
# # Initialize the KMeans model
# kmeans = KMeans(n_clusters=k)
# # Fit the model to the data
# kmeans.fit(X)
# # Get cluster labels for each data point
# cluster_labels = kmeans.labels_
# # Add the cluster labels to your GeoDataFrame
# a['cluster_labels'] = cluster_labels


# one_hot_encoded = pd.get_dummies(new_df['type_isolation_mur_exterieur'], prefix='type_isolation_mur_exterieur')
#
# # # Concatenate the one-hot encoded data with the original GeoDataFrame
# gdf_encoded = pd.concat([new_df, one_hot_encoded], axis=1)
# gdf_encoded.drop(columns=['type_isolation_mur_exterieur'], inplace=True)
#
# from sklearn.cluster import KMeans
#
# gdf_encoded = gdf_encoded.dropna()
# k = 3  # Example number of clusters
# # # Initialize the KMeans model
# kmeans = KMeans(n_clusters=k)
# # # Fit the model to the data
# kmeans.fit(gdf_encoded)
# # # Get cluster labels for each data point
# cluster_labels = kmeans.labels_
# # # Add the cluster labels to your GeoDataFrame
# gdf_encoded['cluster_labels'] = cluster_labels
