import pandas as pd
import os
from connector import data_path
from connector.classes.element_object import Material

trnsys_lib = {'PRGLAY.LIB': dict(delimiter=',', columns=['Group', 'Type', 'Conductivity', 'Capacity', 'Density'],
                                 nb_col=5),
              'Prgwall.LIB': dict(delimiter=':', columns=['MTYPE', 'Description', 'Materials'], nb_col=3),
              'Prgwlay.lib': dict(delimiter=',',
                                  columns=['Type', 'Material', 'Thickness', 'Conductivity', 'Capacity', 'Density'],
                                  nb_col=6)
              }

DEFAULT_MAT_DB = {}

for key, val in trnsys_lib.items():
    df = pd.read_csv(os.path.join(data_path, 'TRNSYS', key), delimiter=val['delimiter'], header=None,
                     encoding='latin1', names=val['columns'], usecols=range(val['nb_col'])
                     )
    if key == 'Prgwall.LIB':
        df['Materials'] = df['Materials'].str.split(';')
        DEFAULT_MAT_DB[key] = df
        continue
    try:

        df['Capacity'] = pd.to_numeric(df['Capacity'], errors='coerce')
        df['Density'] = pd.to_numeric(df['Density'], errors='coerce')
        df['Conductivity'] = pd.to_numeric(df['Conductivity'], errors='coerce')
        df['Conductivity'] *= 0.27778
        df['volumetric_heat_capacity'] = df['Capacity'] * 1000 * df['Density']

        df['Thickness'] = pd.to_numeric(df['Thickness'], errors='coerce')

        DEFAULT_MAT_DB[key] = df

    except KeyError:  # thickness is not included in Prgwall

        DEFAULT_MAT_DB[key] = df

# WALL_MATERIALS = {
#                   }

MATERIALS = {
    'beton': dict(low=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Béton')].loc[74],
                  inter=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Béton')].loc[75],
                  heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Béton')].loc[76],
                  v_heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Béton')].loc[
                      81]),
    'bois': dict(low=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Bois')].loc[97],
                 inter=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Bois')].loc[99],
                 heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Bois')].loc[98],
                 v_heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Bois')].loc[98]),
    'brique': dict(low=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Briq')].loc[0],
                   inter=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Briq')].loc[2],
                   heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Briq')].loc[1],
                   v_heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Briq')].loc[1]),
    'pierre': dict(low=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Calc_dur')].loc[83],
                   inter=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Calc_dur')].loc[
                       83],
                   heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Calc_dur')].loc[
                       83],
                   v_heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Calc_dur')].loc[
                       83]),
    'insulation_wall': dict(
        low=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Poly_')].loc[103],
        inter=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Poly_')].loc[103],
        heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Poly_')].loc[104],
        v_heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Poly_')].loc[104]),
    'plaster': dict(
        low=DEFAULT_MAT_DB['Prgwlay.lib'][DEFAULT_MAT_DB['Prgwlay.lib'].Material.str.contains('ENDUIT_EX')].loc[27],
        inter=DEFAULT_MAT_DB['Prgwlay.lib'][DEFAULT_MAT_DB['Prgwlay.lib'].Material.str.contains('ENDUIT_EX')].loc[
            27],
        heavy=DEFAULT_MAT_DB['Prgwlay.lib'][DEFAULT_MAT_DB['Prgwlay.lib'].Material.str.contains('ENDUIT_EX')].loc[
            27],
        v_heavy=DEFAULT_MAT_DB['Prgwlay.lib'][DEFAULT_MAT_DB['Prgwlay.lib'].Material.str.contains('ENDUIT_EX')].loc[
            27]),
    'ardoise': dict(low=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Ard')].loc[73],
                    inter=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Ard')].loc[73],
                    heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Ard')].loc[73],
                    v_heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Ard')].loc[
                        73]),
    'tuile': dict(low=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Te')].loc[90],
                  inter=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Te')].loc[90],
                  heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Te')].loc[90],
                  v_heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Te')].loc[90]),
    'zinc-aluminium': dict(
        low=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Zin')].loc[113],
        inter=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Zin')].loc[113],
        heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Zin')].loc[113],
        v_heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Zin')].loc[113]),
    'insulation_roof': dict(
        low=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Feut')].loc[100],
        inter=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Feut')].loc[100],
        heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Feut')].loc[100],
        v_heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Feut')].loc[100]),
    'lame_dair': dict(low=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Lame')].loc[67],
                      inter=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Lame')].loc[
                          67],
                      heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Lame')].loc[
                          67],
                      v_heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('Lame')].loc[
                          67]),
    'Roofmate': dict(low=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('STD')].loc[49],
                     inter=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('STD')].loc[49],
                     heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('STD')].loc[49],
                     v_heavy=DEFAULT_MAT_DB['PRGLAY.LIB'][DEFAULT_MAT_DB['PRGLAY.LIB'].Type.str.contains('STD')].loc[
                         49])
}

material_list = ['beton', 'bois', 'brique', 'pierre', 'insulation_wall', 'plaster', 'ardoise', 'tuile',
                 'zinc-aluminium', 'insulation_roof', 'lame_dair', 'Roofmate']

thermal_mat_obj = {}
for mat in material_list:
    thermal_mat_obj[mat] = {}
    for inertia, prop in MATERIALS[mat].items():
        thermal_mat_obj[mat][inertia] = Material(mat, prop.Conductivity, prop.volumetric_heat_capacity)

optic_mat_obj = {
    'beton': dict(albedo=0.2, emissivity=0.92),
    'bois': dict(albedo=0.3, emissivity=0.92),
    'brique': dict(albedo=0.4, emissivity=0.915),
    'pierre': dict(albedo=0.3, emissivity=0.90),
    'ardoise': dict(albedo=0.11, emissivity=0.96),
    'tuile': dict(albedo=0.36, emissivity=0.97),
    'zinc-aluminium': dict(albedo=0.2, emissivity=0.1),
    'plaster': dict(albedo=0.3, emissivity=0.88)
}

PRECAT_ARDOISE = dict(mat_lst=[thermal_mat_obj['ardoise']['low'].__dict__, thermal_mat_obj['lame_dair']['low'].__dict__,
                               thermal_mat_obj['lame_dair']['low'].__dict__,
                               thermal_mat_obj['Roofmate']['low'].__dict__,
                               thermal_mat_obj['Roofmate']['low'].__dict__, thermal_mat_obj['plaster']['low'].__dict__],
                      thckns_lst=[0.01, 0.0001, 0.0001, 0.035, 0.035, 0.013])
PRECAT_ZINC = dict(
    mat_lst=[thermal_mat_obj['zinc-aluminium']['low'].__dict__, thermal_mat_obj['lame_dair']['low'].__dict__,
             thermal_mat_obj['lame_dair']['low'].__dict__, thermal_mat_obj['Roofmate']['low'].__dict__,
             thermal_mat_obj['Roofmate']['low'].__dict__, thermal_mat_obj['plaster']['low'].__dict__],
    thckns_lst=[0.005, 0.0001, 0.0001, 0.025, 0.025, 0.013])
PRECAT_TUILE = dict(mat_lst=[thermal_mat_obj['tuile']['low'].__dict__, thermal_mat_obj['lame_dair']['low'].__dict__,
                             thermal_mat_obj['lame_dair']['low'].__dict__, thermal_mat_obj['Roofmate']['low'].__dict__,
                             thermal_mat_obj['Roofmate']['low'].__dict__, thermal_mat_obj['plaster']['low'].__dict__],
                    thckns_lst=[0.01, 0.0001, 0.0001, 0.035, 0.035, 0.013])
