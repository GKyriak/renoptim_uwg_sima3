import json
import os
<<<<<<< HEAD

from connector.uwg_dict_creation.create_dicts import generate_material_layer_list

from connector import data_path, fn_bld_archetypes, fn_bld_archetypes_properties
from connector.runner.caller_for_bld_archetypes import generate_bld_archetypes
from connector.utils.helpful import check_order

with open(os.path.join(data_path, fn_bld_archetypes)) as f:
    bld_archetypes = json.load(f)

with open(os.path.join(data_path, fn_bld_archetypes_properties)) as f:
    bld_archetypes_properties = json.load(f)


def test_of_floor_height():
    for dep, classes in bld_archetypes.items():
        fl_hgt_list = [k['building']['floor_height'] for k in classes.values()]
        assert any(fl_hgt_list) < 3.5, 'The floor height of {} is too high'.format(dep)
        # assert any(fl_hgt_list) > 2, 'The floor height of {} is too low'.format(dep)



def test_of_u_values_department_behaviour():
=======
from connector.tests import *
from connector.uwg_dict_creation.create_dicts import generate_material_layer_list
from connector import data_path, fn_bld_archetypes, fn_bld_archetypes_properties
# from connector.runner.caller_for_bld_archetypes import generate_bld_archetypes
from connector.utils.helpful import check_order

@pytest.fixture
def bld_archetypes_fixture():
    """This tests can be performed only if the archetypes are calculated"""
    try:

        with open(os.path.join(data_path, fn_bld_archetypes)) as f:
            bld_archetypes = json.load(f)

        with open(os.path.join(data_path, fn_bld_archetypes_properties)) as f:
            bld_archetypes_properties = json.load(f)

    except FileNotFoundError:

        raise FileNotFoundError("You must first calculate the building archetypes.Run `execute_preprocessing.py`")

    except json.decoder.JSONDecodeError:

        raise json.decoder.JSONDecodeError("The json files where not properly dumped.")

    return bld_archetypes, bld_archetypes_properties


def test_of_floor_height(bld_archetypes_fixture):
    """
    Test if the floor heights of building archetypes are within a consistent range.
    """
    bld_archetypes, _ = bld_archetypes_fixture
    for dep, classes in bld_archetypes.items():
        fl_hgt_list = [k['building']['floor_height'] for k in classes.values()]
        assert all(fl < 3.6 for fl in fl_hgt_list), \
            'The floor height of {} is too high'.format(dep)

        assert all(fl > 2. for fl in fl_hgt_list), \
            'The floor height of {} is too low'.format(dep)


def test_of_u_values_department_behaviour(bld_archetypes_fixture):

    _, bld_archetypes_properties = bld_archetypes_fixture
>>>>>>> 5eceffd77136b1b165f7c4d985864e5e6e7d8b43

    elem_to_check = ['mean_w_uv', 'mean_w_uroof']
    for surface in elem_to_check:

        test_flag = 0
        for dep, classes in bld_archetypes_properties.items():
            uv_list = [k[surface] for k in classes.values()]
            if check_order(uv_list):
                continue
            else:
                test_flag += 1
                print('The Uvalues of {} in department {} might be incorrect'.format(surface, dep))

        score = (len(bld_archetypes_properties) - test_flag) / len(bld_archetypes_properties) * 100
<<<<<<< HEAD

        assert score > 90, '{}% of the departments present U-Values with a kind of error for {}'.format(score, surface)


def test_of_u_values_limits():
=======
        if surface == 'mean_w_uv':
            assert score > 80, '{}% of the departments present U-Values with a kind of error for {}'.format(score, surface)
        else:
            assert score > 51, '{}% of the departments present U-Values with a kind of error for {}'.format(score,
                                                                                                            surface)
            # It is sufficient for roofs as we use roof archetypes


def test_of_u_values_limits(bld_archetypes_fixture):

    _, bld_archetypes_properties = bld_archetypes_fixture

>>>>>>> 5eceffd77136b1b165f7c4d985864e5e6e7d8b43
    elem_to_check = ['mean_w_uv', 'mean_w_uroof']
    for surface in elem_to_check:

        for dep, classes in bld_archetypes_properties.items():
            uv_list = [k[surface] for k in classes.values()]

            assert all(uv < 2.2 for uv in uv_list), \
                'The Uvalues of {} in department {} are too high'.format(surface, dep)

            assert all(uv > 0 for uv in uv_list), \
                'The Uvalues of {} in department {} are too low'.format(surface, dep)


<<<<<<< HEAD
def test_of_rprsentative_bld_ratio():
=======
def test_of_rprsentative_bld_ratio(bld_archetypes_fixture):

    _, bld_archetypes_properties = bld_archetypes_fixture
>>>>>>> 5eceffd77136b1b165f7c4d985864e5e6e7d8b43

    test_flag = 0
    for dep, classes in bld_archetypes_properties.items():
        bld_rto_list = [k['rprs_bld_rto'] for k in classes.values()]
        if sum(bld_rto_list) > 55:
            continue
        else:
            test_flag += 1
            print('The building ratio is not representative for department {}'.format(dep))

    score = (len(bld_archetypes_properties) - test_flag) / len(bld_archetypes_properties) * 100

    print('The ratio of departments with representative buildings is:', score)

    assert score > 90, '{}% of the departments present classified building ratio less than 51%'.format(score)

# generate_bld_archetypes(['75'])


<<<<<<< HEAD
def test_of_rprsentative_hgt():
=======
def test_of_rprsentative_hgt(bld_archetypes_fixture):

    _, bld_archetypes_properties = bld_archetypes_fixture
>>>>>>> 5eceffd77136b1b165f7c4d985864e5e6e7d8b43

    for dep, classes in bld_archetypes_properties.items():
        hgt_list = [k['mean_w_hgt'] for k in classes.values()]

        assert all(hgt < 24 for hgt in hgt_list), \
            'The building height in department {} is too high'.format(dep)

        assert all(uv > 0 for uv in hgt_list), \
            'The building height in department {} is too low'.format(dep)


def test_generate_material_layer_list():
    u_value_def = 0.22

    default_val = {'insulation_type_wall': 'ITI',
                   'insul_thickness': 5.,
                   'wall_material': 'beton',
                   'wall_thickness': 20.,
                   'cooling_generator': 'pac air / air',
                   'insulation_type_roof': 'ITI',
                   'insulation_type_floor': 'ITI',
                   'roof_mat': 'beton',
                   'inertia': 'Légère'
                   }

<<<<<<< HEAD
    layer_thickness_lst_wall_low, material_lst_wall_low = generate_material_layer_list(u_value_def, default_val, elem_type='wall')
=======
    layer_thickness_lst_wall_low, material_lst_wall_low = generate_material_layer_list(u_value_def, default_val,
                                                                                       elem_type='wall')
>>>>>>> 5eceffd77136b1b165f7c4d985864e5e6e7d8b43

    assert material_lst_wall_low[1]['name'] == material_lst_wall_low[2]['name'] == 'beton'
    assert material_lst_wall_low[3]['name'] == material_lst_wall_low[4]['name'] == 'insulation_wall'
    assert sum(layer_thickness_lst_wall_low[3:5]) == 0.05  # corresponds to 5cm insulation thickness as in def_val
    assert sum(layer_thickness_lst_wall_low[1:3]) == 0.15 # corresponds to wall_thickness -  insulation thickness as in def_val

    u_value_high = 1.5

<<<<<<< HEAD
    layer_thickness_lst_wall_high, material_lst_wall_high = generate_material_layer_list(u_value_high, default_val, elem_type='wall')
=======
    layer_thickness_lst_wall_high, material_lst_wall_high = generate_material_layer_list(u_value_high, default_val,
                                                                                         elem_type='wall')
>>>>>>> 5eceffd77136b1b165f7c4d985864e5e6e7d8b43

    assert material_lst_wall_high[1]['name'] == material_lst_wall_high[2]['name'] == 'beton'
    assert material_lst_wall_high[3]['name'] == material_lst_wall_high[4]['name'] == 'insulation_wall'

    u_value_vhigh = 2.9

<<<<<<< HEAD
    layer_thickness_lst_wall_vhigh, material_lst_wall_vhigh = generate_material_layer_list(u_value_vhigh, default_val, elem_type='wall')

    assert material_lst_wall_vhigh[1]['name'] == material_lst_wall_vhigh[2]['name'] == 'beton'
    assert material_lst_wall_vhigh[3]['name'] == material_lst_wall_vhigh[4]['name'] == 'insulation_wall'
=======
    layer_thickness_lst_wall_vhigh, material_lst_wall_vhigh = generate_material_layer_list(u_value_vhigh, default_val,
                                                                                           elem_type='wall')
>>>>>>> 5eceffd77136b1b165f7c4d985864e5e6e7d8b43

    assert sum(layer_thickness_lst_wall_low[3:5]) > sum(layer_thickness_lst_wall_high[3:5])

    assert sum(layer_thickness_lst_wall_low[1:3]) > sum(layer_thickness_lst_wall_high[1:3])

    assert sum(layer_thickness_lst_wall_low[3:5]) > sum(layer_thickness_lst_wall_vhigh[3:5])

    assert sum(layer_thickness_lst_wall_low[1:3]) > sum(layer_thickness_lst_wall_vhigh[1:3])

    assert sum(layer_thickness_lst_wall_high[3:5]) == sum(layer_thickness_lst_wall_vhigh[3:5])

    assert sum(layer_thickness_lst_wall_high[1:3]) > sum(layer_thickness_lst_wall_vhigh[1:3])

    u_value_error = 5.9

    try:
        generate_material_layer_list(u_value_error, default_val, elem_type='wall')
    except ValueError:
        print('\n For U-Value = {} the threshold value of the iterator is overpassed as expected.'.format(u_value_error
                                                                                                          ))