DEP_LIST = [str(el).zfill(2) for el in range(1, 96)]
DEP_LIST.remove('20')
DEP_LIST.extend(["2A", "2B"])
DEP_LIST.sort()

DPE_BINS = ['A', 'B', 'C', 'D', 'E', 'F', 'G']

# BDNB unique description for insulation, wall materials, roof materials
# insulation_position = ['inconnu',
#                        'isole',
#                        'ITE',
#                        'ITE+ITR',
#                        'ITI',
#                        'ITI+ITE',
#                        'ITI+ITR',
#                        'ITR',
#                        'non isole',
#                        'NULL']
#
# wall_materials = ['autre',
#                   'beton',
#                   'bois',
#                   'brique',
#                   'pierre',
#                   '[NULL]']
#
#
# roof_materials = ['ardoise',
#                   'autre',
#                   'beton',
#                   'tuile',
#                   'zinc-aluminium',
#                   '[NULL]'
#                   ]
