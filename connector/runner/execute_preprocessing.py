from connector.utils.constants import DEP_LIST
from connector import data_path, fn_bld_archetypes
from connector.runner.caller_for_bld_archetypes import generate_bld_archetypes
from connector.runner.caller_for_sp_units_completion import fill_in_spatial_units_params
from connector.sql_queries.request_spatial_units import generate_spatial_units, request_spatial_units
import os


generate_spatial_units(log_time={})

if os.path.exists(os.path.join(data_path, fn_bld_archetypes)):
    pass
else:
    generate_bld_archetypes(DEP_LIST, save_option=True)

uwg_sp_units = request_spatial_units(log_time={})
fill_in_spatial_units_params(uwg_sp_units, save_file=True)

