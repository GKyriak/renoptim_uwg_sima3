Build spatial units
*******************

This process is subdivided in several parts encapsulated in the following function:
:func:`connector/sql_queries/request_spatial_units`

1. Select the spatial units (G-IRIS) that the ICU4BDNB tool has identified as hazard boroughs

    1.1. Associate the corresponding IRIS and the nearest meteorological station file name

    1.2. Add the urban greenery and distinguish it in `tall` and `short` vegetation

        1.2.1. Add the rural vegetation coverage for the corresponding meteo file

Build building archetypes
*************************
**2. Generate the building archetypes for each department and DPE class**

    2.1. Request for each department the building elements from BDNB through
    :func:`connector/sql_queries/request_global_buildings`.

    2.2. Generate the representative buildings :func:connector/runner/caller_for_bld_archetypes/generate_rprsntive_bld_dep_dpe`

        2.2.1. Apply filters:
            1. 1st criterion == Filter by DPE class
                1.1. Calculate the weighted average U value
            2. 2nd criterion == Filter by U value

    2.3. Extract building weighted parameters (floor height, glazing ratio, solar heat gains coeff, window U-Value, etc...)

    2.4. Identify representative envelope characteristics, such as:
        1. insulation wall position
        2. insulation wall thickness
        3. wall material type
        4. wall thickness
        5. cooling generator
        6. insulation roof position
        7. insulation floor position
        8. roof material type
        9. inertia level
        10. construction period

    2.5. Generate the wall / roof material lists including material type, position, and thicknesses based on a given representative wall U-Value

    2.6. Create the necessary UWG objects representing `Building` and `Elements` by exploiting

    .. py:function:: connector/classes/building_object/Building

    .. automodule:: connector.classes.building_object.Building

and

.. py:function:: connector/classes/building_object/BuildingObject

.. automodule:: connector.classes.building_object.BuildingObject

and

.. py:function:: connector/classes/element_object/Element

.. automodule:: connector.classes.element_object.Element



**3. Dump the building archetype file**

    3.1 :func:`connector/runner/caller_for_bld_archetypes.dump_bld_archetypes_dicts`


Complete spatial units
***********************

Each spatial unit is enhanced with several required parameters:
    * Building density
    * Vertical to horizontal ratio (detection of adjacency included)
    * etc (see: below)

and the final UWG object is constructed

.. py:function:: connector/classes/uwg_object/UwgOject

.. automodule:: connector.classes.uwg_object.UwgOject

We then dump the spatial units (`_uwg_dic.json`) dictionary including for each computational grid, 3 parameters:

    1. the referenced meteo file name (import data of UWG)
    2. The uwg parameter file: dictionary with 51 input parameters
    3. A boolean for allowing calculation of not