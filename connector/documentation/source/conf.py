# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html
# import sys, os
# sys.path.append(os.path.join(os.path.abspath('../../'), 'uhi_intensity'))


# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'uhi renoptim'
copyright = '2024, G-E Kyriakodis'
author = 'G-E Kyriakodis'
release = 'v0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['sphinx.ext.autodoc',
              ]

templates_path = ['_templates']
exclude_patterns = []

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'
# html_theme = 'classic'
# html_theme_options = {
#     "sidebarbgcolor": '#222831',
#     "sidebarbtncolor": '#222831',
#     "textcolor": '#222831',
# }
html_static_path = ['_static']


todo_include_todos = True

# -- Options for autodoc extension --------------------------------------------
autodoc_default_options = {
    'inherited-members': True,
}

autodoc_member_order = 'groupwise'


# def setup(app):
#     """Run custom code with access to the Sphinx application object
#     Args:
#         app: the Sphinx application object
#     """
#
#     a = None
#     import pdb; pdb.set_trace()
