Meteo
-----


The meteorological data correspond to free climate data for building performance simulation,
derived from several public sources in EnergyPlus weather (.epw) file format [Crawley1998]_.

229 Typical Meteorological Year `TMY <https://climate.onebuilding.org/default.html>`_ datasets of the most recent 15 years (2007-2021)
covering the entire country (`Figure:` :ref:`meteo_png`).

.. figure:: ../images/meteo.png
   :align: center
   :width: 400
   :name: meteo_png
   :figclass: align-center

   Geocoding points of the employed TMY 2007-2021 files covering the entire country.


.. warning::

   EnergyPlus weather: is the only file format supported by the package.


References
^^^^^^^^^^

.. _Crawley1998:
Crawley, Drury B. "Which weather data should you use for energy simulations of commercial buildings?." Transactions-American society of heating refrigerating and air conditioning engineers 104 (1998): 498-515.
