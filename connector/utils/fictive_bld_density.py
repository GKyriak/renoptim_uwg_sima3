def generate_fictive_bld_density(blddensity: float, treecover: float, grasscover: float):
    """
    Iteration over building density tree cover and grass cover to verify their convergence.
    It is used for the fictive rural cells.

    :param blddensity: (0-1) Building footprint density as fraction of urban area.
    :param treecover: (0-1) Fraction of urban ground covered in trees.
    :param grasscover: (0-1) Fraction of urban ground covered in grass.

    :return:
    """

    iter_factor = 0.01
    land_cover = blddensity + treecover + grasscover

    while land_cover > 1:

        if blddensity > 0.1:
            blddensity -= iter_factor
            land_cover = blddensity + treecover + grasscover
        else:
            if treecover > grasscover:
                treecover -= iter_factor
                land_cover = blddensity + treecover + grasscover

            else:
                grasscover -= iter_factor
                land_cover = blddensity + treecover + grasscover

    return blddensity, treecover, grasscover


def check_validity_bld_density(blddensity: float, treecover: float, grasscover: float):
    """
    Checks if sum of building footprint, and vegetation are lower than 1.

    :param blddensity: (float) (0-1) Building footprint density as fraction of urban area.
    :param treecover: (float) (0-1) Fraction of urban ground covered in trees.
    :param grasscover: Fraction of urban ground covered in grass.

    :return:
    """

    if blddensity + treecover + grasscover < 1:
        return True
    else:
        return False
