import datetime
import time
import pytz
import geopandas as gpd


def timeit(method):
    """
    Log of execution time of a function
    """

    def timed(*args, **kw):
        if 'log_time' in kw:

            ts = time.time()
            print(
                f'Start {method.__name__} : {datetime.datetime.now(pytz.timezone("Europe/Paris")).strftime("%H:%M:%S")}'
            )
            result = method(*args, **kw)
            te = time.time()

            name = kw.get('log_name', method.__name__.upper())
            delta = (datetime.datetime.min + datetime.timedelta(seconds=int(te - ts))).time().strftime("%H:%M:%S")
            kw['log_time'][name] = delta

            print(f'    execution time : {delta}')
        else:
            result = method(*args, **kw)
        return result

    timed.__doc__ = method.__doc__
    return timed


def query_dict_to_gpd(data: dict, **kwargs):

    sort_col_nm = kwargs.get('sort')

    if not data:
        return gpd.GeoDataFrame()

    records = [value for key, value in data.items()]

    gdf = gpd.GeoDataFrame(records, crs='EPSG:2154').sort_values(by=sort_col_nm).reset_index(drop=True)

    return gdf


def check_order(lst):
    for i in range(len(lst) - 1):
        if lst[i] > lst[i + 1]:
            return False
    return True