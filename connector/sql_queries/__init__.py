import yaml
import os
from connector import data_path, config_path, bdnb_tables_fn

with open(os.path.join(data_path, config_path, bdnb_tables_fn), 'r') as stream:
    try:
        bdnb_tables = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)

for key, value in bdnb_tables.items():
    globals()[key] = value
