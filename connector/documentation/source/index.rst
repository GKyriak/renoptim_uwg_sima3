.. UHI Intensity documentation master file, created by
   sphinx-quickstart on Thu Feb 16 16:19:15 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to uhi renoptim documentation!
#########################################


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   installation
   data
   usage
   pipeline
   contribute
   license

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
