Usage
=====

Here's a *5-step* example on how to use **UWG Renoptim**:

1. Import uwg_renoptim sub-modules:

    >>> from connector.utils.constants import DEP_LIST
    >>> from connector import data_path, fn_bld_archetypes
    >>> from connector.runner.caller_for_bld_archetypes import generate_bld_archetypes
    >>> from connector.runner.caller_for_sp_units_completion_new import fill_in_spatial_units_params
    >>> from connector.sql_queries.request_spatial_units import generate_spatial_units, request_spatial_units
    >>> import os
    >>> import json
    >>> from concurrent.futures import ProcessPoolExecutor
    >>> from uwg import UWG
    >>> from connector import uwg_param_files_path, results_path, DEFAULT_EPW_PATH

2. Generate the spatial units:

    >>> generate_spatial_units(log_time={})

3. Generate the building archetypes:
    >>> generate_bld_archetypes(DEP_LIST, save_option=True)

4. Complete the spatial units parameters and produce the UWG input files:

    >>> fill_in_spatial_units_params(request_spatial_units(log_time={}), save_file=True)

5. Execute the UWG calculation:

    >>> from connector.runner import execute_uwg


