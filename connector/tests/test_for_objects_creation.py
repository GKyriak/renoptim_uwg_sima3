from connector.utils.schedules import create_uwg_schedules_RT
from connector.classes.uwg_object import UwgOject
from connector.tests import *


@pytest.fixture
def default_object():
    """This tests can be performed only if the archetypes are calculated"""
    procat_dict = {
        "type": "UWG",
        "location": "C",
        "bldheight": 3,
        "blddensity": max(0.1, 0.6),
        "flr_h": 3,
        "vertohor": 0.01,
        "charlength": 300,
        "paralLength": 300,
        "orthLength": 300,
        "urbArea": 300*300,
        "perimeter": 300*4,
        "bld": [["midriseapartment", "new", 1.0]],
        "schtraffic": [
        [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.7, 0.8, 1.0, 0.8, 0.8, 1.0, 0.7,
         0.7, 0.8, 0.8, 0.8, 1.0, 0.8, 0.5, 0.5, 0.2, 0.2, 0.2],  # Weekday

        [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.5, 0.5, 0.5, 0.5, 0.3, 0.5, 0.5,
         0.5, 0.5, 0.5, 0.3, 0.5, 0.5, 0.3, 0.3, 0.2, 0.2, 0.2],  # Saturday

        [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.3, 0.3, 0.5, 0.3, 0.5, 0.3,
         0.5, 0.3, 0.5, 0.3, 0.5, 0.3, 0.5, 0.2, 0.2, 0.2, 0.2]  # Sunday
                ],
        "albroof": 0.25,
        "vegroof": 0.0,
        "glzr": 0.1,
        "albwall": 0.25,
        "shgc": 0.4,
        "month": 1,
        "day": 1,
        "nday": 365,
        "dtsim": 300,
        "dtweather": 3600,
        "autosize": 0,
        "sensocc": 100,
        "latfocc": 0.3,
        "radfocc": 0.2,
        "radfequip": 0.5,
        "radflight": 0.7,
        "h_ubl1": 1000,
        "h_ubl2": 80,
        "h_ref": 150,
        "h_temp": 2,
        "h_wind": 10,
        "c_circ": 1.2,
        "c_exch": 1,
        "maxday": 150,
        "maxnight": 20,
        "windmin": 1,
        "h_obs": 0.1,
        "h_mix": 1,
        "albroad": 0.2,
        "droad": 0.5,
        "kroad": 1,
        "croad": 1600000,
        "sensanth": 0,
        "zone": "1A",
        "grasscover": 0.3,
        "treecover": 0.1,
        "vegstart":4,
        "vegend": 11,
        "albveg": 0.25,
        "latgrss": 0.4,
        "lattree": 0.6,
        "rurvegcover": 0.9
    }

    return procat_dict


def test_uwg_object_cretion(default_object):
    """
    Test if the uwg object is properly created and check values based on default ones.
    """
    uwg_dic = default_object

    ref_bem_vector = [{"type": "BEMDef",
                       "building": {"type": "Building", "floor_height": 3.5, "int_heat_night": 1, "int_heat_day": 10,
                                    "int_heat_frad": 0.1, "int_heat_flat": 0.1, "infil": 0.26, "vent": 0.0004,
                                    "glazing_ratio": 0.3, "u_value": 2.44, "shgc": 0.44, "condtype": "AIR", "cop": 5.2,
                                    "coolcap": 40, "heateff": 0.7, "initial_temp": 293},
                       "wall": {"type": "Element", "albedo": 0.2, "emissivity": 0.92,
                                "layer_thickness_lst": [0.002, 0.075, 0.075, 0.015, 0.015, 0.002], "material_lst": [
                               {"type": "Material", "name": "plaster", "thermalcond": 1.1527870000000002,
                                "volheat": 1700000.0},
                               {"type": "Material", "name": "beton", "thermalcond": 0.33083598000000003,
                                "volheat": 704000.0},
                               {"type": "Material", "name": "beton", "thermalcond": 0.33083598000000003,
                                "volheat": 704000.0},
                               {"type": "Material", "name": "insulation_wall", "thermalcond": 0.029166900000000003,
                                "volheat": 41300.0},
                               {"type": "Material", "name": "insulation_wall", "thermalcond": 0.029166900000000003,
                                "volheat": 41300.0},
                               {"type": "Material", "name": "plaster", "thermalcond": 1.1527870000000002,
                                "volheat": 1700000.0}], "vegcoverage": 0.0, "t_init": 293, "horizontal": False,
                                "name": "MassWall"},
                       "mass": {"type": "Element", "albedo": 0.2, "emissivity": 0.9,
                                "layer_thickness_lst": [0.002, 0.025, 0.025, 0.025,
                                                        0.025, 0.002], "material_lst": [
                               {"type": "Material", "name": "plaster", "thermalcond": 1.1527870000000002,
                                "volheat": 1700000.0},
                               {"type": "Material", "name": "beton", "thermalcond": 0.16055684, "volheat": 352000.0},
                               {"type": "Material", "name": "beton", "thermalcond": 0.16055684, "volheat": 352000.0},
                               {"type": "Material", "name": "beton", "thermalcond": 0.16055684, "volheat": 352000.0},
                               {"type": "Material", "name": "beton", "thermalcond": 0.16055684, "volheat": 352000.0},
                               {"type": "Material", "name": "plaster", "thermalcond": 1.1527870000000002,
                                "volheat": 1700000.0}],
                                "vegcoverage": 0.0, "t_init": 293, "horizontal": True,
                                "name": "MassFloor"},
                       "roof": {"type": "Element", "albedo": 0.2, "emissivity": 0.1,
                                "layer_thickness_lst": [0.005, 0.0001, 0.0001, 0.025, 0.025, 0.013], "material_lst": [
                               {"type": "Material", "name": "zinc-aluminium", "thermalcond": 113.31812876000001,
                                "volheat": 2758800.0},
                               {"type": "Material", "name": "lame_dair", "thermalcond": 0.06000048, "volheat": 1227.0},
                               {"type": "Material", "name": "lame_dair", "thermalcond": 0.06000048, "volheat": 1227.0},
                               {"type": "Material", "name": "Roofmate", "thermalcond": 0.03000024, "volheat": 42876.0},
                               {"type": "Material", "name": "Roofmate", "thermalcond": 0.03000024, "volheat": 42876.0},
                               {"type": "Material", "name": "plaster", "thermalcond": 1.1527870000000002,
                                "volheat": 1700000.0}], "vegcoverage": 0.0, "t_init": 293, "horizontal": True,
                                "name": "MassRoof"}, "bldtype": "C", "builtera": "new"}]


    schedules = [create_uwg_schedules_RT(k['bldtype'], k['builtera']) for k in ref_bem_vector]
    uwg = UwgOject('1', default_object['bldheight'], default_object['blddensity'], default_object['flr_h'],
                   default_object['vertohor'], default_object['charlength'],
                   default_object['paralLength'],
                   default_object['orthLength'],
                   default_object['urbArea'],
                   default_object['perimeter'],
                   default_object['bld'],
                   default_object['albroof'], 0, default_object['glzr'],
                   default_object['albwall'], default_object['shgc'],
                   1000, 80, 150, 0.2, 0,
                   default_object['grasscover'], default_object['treecover'], default_object['rurvegcover'],
                   schedules, ref_bem_vector
                   )

    param_dic = uwg.__dict__

    assert type(param_dic) == dict

    assert len(param_dic) > 57

    for key, val in default_object.items():
        if key in param_dic:
            assert val == param_dic[key]
