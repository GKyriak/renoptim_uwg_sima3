# -*- coding: utf-8 -*-
"""
Created on 2022
Author: @BDNB code
"""
from connector import PG_ROLE

role = PG_ROLE


def exec_sqlalchemy_query(engine, role, query, type=None):
    """
    Wrapper of SQL alchemy session transaction, following BDNB code.

    :param engine: (Engine) for activating connection to the PostGIS database
    :param role: (str) set to default: gorenove_creator not used in this implementation
    :param query: (str) PostgreSQL query to be processed
    :param type: (str) option for fetch and vacuum. Default set to None for regular queries

    :return: Executes the requested query
    """

    with engine.connect() as conn:
        conn = conn.connection
        cursor = conn.cursor()

        # cursor.execute(f"SET ROLE {role};")
        try:
            cursor.execute(query)
        except Exception as e:
            # logging.exception(query)
            if type == 'vacuum':
                # TODO: find a better way to implement this maybe a class DB is required
                do_vacuum(conn, cursor, query)
                conn.commit()
                return
            conn.commit()
            raise

        if type == 'fetchall':
            result = cursor.fetchall()
        elif type == 'fetchone':
            result = cursor.fetchone()
        else:
            # conn.execute(query)
            result = None

        conn.commit()

    return result


def do_vacuum(conn, cursor, query: str):
    """

    :param conn:
    :param cursor:
    :param query: (str) SQL VACUUM query
    :return:
    """
    old_isolation_level = conn.isolation_level
    conn.set_isolation_level(0)
    # query = "VACUUM FULL"
    cursor.execute(query)
    conn.set_isolation_level(old_isolation_level)

    return
