Contributing
------------
We welcome contributions from anyone (inside CSTB for the moment).

### Code contribution
This project follows CSTB general contributing guideline.

### Issues
Feel free to submit issues and enhancement requests to the repository.

### Contributing

In general, we follow the "fork-and-pull" Git workflow.

    Fork the repo on SCM
    Clone the project to your own machine
    Commit changes to your own branch
    Push your work back up to your fork
    Submit a Pull request so that we can review your changes

### Be nice!
Please note that the developer of this project is overloaded!!

### Thank you and happy coding! 
:)