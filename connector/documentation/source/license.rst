License
-------

See the `LICENSE` file for more information by clicking on this :file:`../LICENSE` link.

Publication ethics
------------------

Any publication resulting from the use of the module must contain the main developer.






