"""UWG Renoptim Connector"""

__author__ = 'Georgios KYRIAKODIS'
__version__ = '0.0.1'
__license__ = 'GNU GENERAL PUBLIC LICENSE'
__copyright__ = 'Copyright 2024 CSTB'

import yaml
import os

root_directory = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
config_path = 'BDNB'
config_fn = 'pg_config.yml'
bdnb_tables_fn = 'bdnb_tables.yml'

data_path = os.path.join(root_directory, 'connector', 'data')
if not os.path.exists(data_path):
    os.makedirs(data_path)

uwg_param_files_path = os.path.join(data_path, 'uwg_param_files')
if not os.path.exists(uwg_param_files_path):
    os.makedirs(uwg_param_files_path)

results_path = os.path.join(data_path, 'Results')
if not os.path.exists(results_path):
    os.makedirs(results_path)

ref_meteo_path = os.path.join(data_path, 'meteo')
if not os.path.exists(ref_meteo_path):
    os.makedirs(ref_meteo_path)

fn_bld_archetypes = 'bld_archetypes_dep_level.json'
fn_bld_archetypes_properties = 'bld_archetypes_properties_dep_level.json'

DEFAULT_EPW_PATH = os.path.join(ref_meteo_path, 'climateOneBuilding')

with open(os.path.join(data_path, config_path, config_fn), 'r') as stream:
    try:
        data = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)

if len(data) != 7:
    raise IndexError("Expecting a dict with 7 keys to allow connection to the BDNB but "
                     "recieved: {}".format(len(data)))

for key, val in data.items():
    if not isinstance(val, str) or not val.strip():
        raise TypeError("Expected a string but received None or empty string for: {}".format(key))

HOST = data.get('HOST')
USER = data.get('USER')
PORT = data.get('PORT')
DBNAME = data.get('DBNAME')
PASSWORD = data.get('PASSWORD')
ACTIVE_SCHEMA = data.get('ACTIVE_SCHEMA')
PG_ROLE = data.get('PG_ROLE')
