.. image:: https://img.shields.io/badge/python-3.10-blue
   :alt: Python version badge

.. image:: https://img.shields.io/badge/PostgreSQL-16.0-yellow
   :target: https://www.postgresql.org/

.. image:: https://img.shields.io/badge/GEOS-3.9.0-red
   :target: https://trac.osgeo.org/geos/

Installation
------------

Local Development
*****************

*  Clone this repo locally

.. code-block:: console

    $ git clone git@scm.cstb.fr:dee/renoptim/uwg_renoptim.git

or

.. code-block:: console

    $ git clone https://scm.cstb.fr/dee/renoptim/uwg_renoptim.git

*  Create the environment:

Open a command line tool in the uhi_intensity root folder, enter the
``ci`` folder, and then run in the terminal
the following :

.. code-block:: console

    $ conda env create -f environment.yml

To remove an existing environment:

.. code-block:: console

    $ conda remove --name uhi_renoptim --all


Please refer to the ``../../requirements.txt`` file for detailed package requirements.

* Activate the uhi_renoptim:

.. code-block:: console

    $ conda activate uhi_renoptim


*  To install *uhi_renoptim*, revert to project's root folder and do the following:

.. code-block:: console

    $ python setup.py install

Documentation
*************

To generate the documents on your local machine, navigate to the root folder and execute the following commands.

* Install dependencies

.. code-block:: console

    $ pip install Sphinx sphinxcontrib-fulltoc sphinx_bootstrap_theme

* Generate rst files for modules

.. code-block:: console

    $ sphinx-apidoc -f -e -d 4 -o ./connector/documentation/doc_modules ./connector

* Build the documentation under `_build/docs folder`

.. code-block:: console

    $ sphinx-build -b html ./connector/documentation/source ./connector/documentation/_build/docs

Alternatively, run the command:

.. code-block:: console

    $ .\make.bat html

or, for more detailed build to enable additional warnings and possible errors

.. code-block:: console

    $ sphinx-build -n source build

BDNB Requirements
*****************

To allow the tool to communicate with the `gorenove_fabric` database
and `create/drop/fetch` any new/existing table, the user
must have an active account in the BDNB.
To request an account activation and an ssh connection please create a
`git issue
<https://scm.cstb.fr/dee/bdnb/utiliser-la-bdnb/-/issues/new?issuable_template=ajout_utilisateur>`_
and contact the BDNB team.

Once the BDNB account is activated, the user should request access to ``sat4bdnb`` schema.

After receiving the necessary permissions, the user should go to the ``ci/pg_config.yml`` file and modify the connection credentials
to reflect the new account information.

Here is an example of the `pg_config.yml` file in which the user should modify the ``USER`` and ``PASSWORD`` parameters
to match their BDNB account information:

* HOST = 'localhost'
* USER = 'myusername666'
* PORT = '35432'
* DBNAME = 'gorenove_fabric'
* PASSWORD = 'mypassword'
* ACTIVE_SCHEMA = 'yourschema'
* PG_ROLE = 'gorenove_creator'

Make sure to replace the placeholders ``USER``, ``ACTIVE_SCHEMA``  and ``PASSWORD`` with your own `BDNB` account information.

After this step you are ready to connect to the *BDNB* environment and use the *uhi renoptim* package.
