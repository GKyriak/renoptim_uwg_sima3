import os
import json
from concurrent.futures import ProcessPoolExecutor
from uwg import UWG
from connector import uwg_param_files_path, results_path, DEFAULT_EPW_PATH


def process_grid(grid: str, grid_params: dict):
    """
    This script executes the UWG simulation for each simulation grid

    :param grid: number of the unique identifier of each uwg grid
    :param grid_params: uwg type parameter file & corresponding meteo file name

    :return: Stores a new .epw file in data/Results for each grid
    """

    default_epw_path = DEFAULT_EPW_PATH

    new_epw_path = results_path

    try:

        epw_base_fn = grid_params['meteo_fn'].split('.epw')[0]
        epw_new_fn = f"{grid}_{epw_base_fn}_UWG.epw"
        epw_new_full_path = os.path.join(new_epw_path, epw_new_fn)

        model = UWG.from_dict(grid_params['uwg_param'],
                              epw_path=os.path.join(default_epw_path, grid_params['meteo_fn']),
                              new_epw_name=epw_new_full_path)
        model.generate()
        model.simulate()
        model.write_epw()

    except Exception:
        if os.path.isfile(epw_new_full_path):
            os.remove(epw_new_full_path)


if __name__ == "__main__":

    uwg_dic_path = os.path.join(uwg_param_files_path, '_uwg_dic_new.json')

    with open(uwg_dic_path) as f:
        datadic = json.load(f)

    with ProcessPoolExecutor() as executor:
        # Submit tasks for each grid
        futures = [executor.submit(process_grid, grid, grid_params)
                   for grid, grid_params in datadic.items() if grid_params['simulation'] == True

                   ]

        for future in futures:
            future.result()

    # In this step we recover the grids that the simulation has not converged, and we increase the simulation time steps

    print('\n Processing the problematic grids')

    # total_grids = set(first_5_elements.keys())
    total_grids = set(datadic.keys())

    grids_treated = set(k.split('_')[0] for k in os.listdir(results_path))
    problem_list = total_grids - grids_treated

    problem_dict = {key: value for key, value in datadic.items() if key in problem_list
                    }

    for grid in problem_list:
        problem_dict[grid]['uwg_param']['dtsim'] = 150

    print('\n Nb of problematic grids:', len(problem_dict))

    with ProcessPoolExecutor() as executor:
        # Submit tasks for each grid
        futures = [executor.submit(process_grid, grid, grid_params) for grid, grid_params in problem_dict.items()
                   if grid_params['simulation'] == True
                   ]

        # Wait for all tasks to complete
        for future in futures:
            future.result()

    print('\n Processing the problematic grids last time')

    total_grids = set(datadic.keys())

    grids_treated = set(k.split('_')[0] for k in os.listdir(results_path))
    problem_list = total_grids - grids_treated

    problem_dict = {key: value for key, value in datadic.items() if key in problem_list
                    }

    for grid in problem_list:
        problem_dict[grid]['uwg_param']['dtsim'] = 75

    print('\n Nb of problematic grids:', len(problem_dict))

    with ProcessPoolExecutor() as executor:
        # Submit tasks for each grid
        futures = [executor.submit(process_grid, grid, grid_params) for grid, grid_params in problem_dict.items()]

        # Wait for all tasks to complete
        for future in futures:
            future.result()
