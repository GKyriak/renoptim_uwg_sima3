# -*- coding: utf-8 -*-
"""
Created on 2024
Author: @GKyriakodis
"""

import sqlalchemy
from shapely import wkb
import geopandas as gpd
from connector import HOST, USER, PORT, DBNAME, PASSWORD, ACTIVE_SCHEMA, PG_ROLE
from connector.runner.caller_for_bld_archetypes import convert_decimal_to_float
from connector.utils.sql_general_functions import exec_sqlalchemy_query
from connector.utils.helpful import timeit, query_dict_to_gpd
from connector.sql_queries import sp_un_table, iuhi_table, iris_table, weather_ind_table, \
    init_grid_table, oso_table, uasl_table, pop_table, table_bat_cstr, table_bat_syn_env, table_bat_topo, table_bat_dpe
from connector.utils.helpful import timeit


host = HOST
user = USER
port = PORT
dbname = DBNAME
password = PASSWORD
active_schema = ACTIVE_SCHEMA
database_url = f"postgresql://{user}:{password}@{host}:{port}/{dbname}"
engine = sqlalchemy.create_engine(database_url)
param_file_dic = {}


@timeit
def generate_spatial_units():
    """
    This query produces the spatial calculation domain of the UHI Indicator, including:
       * uwg_sp_un_id : Identifier of each spatial layer
       * code_iris : Unique Identifier of each IRIS spatial layer that intersects with the uwg spatial unit
       * label_iris : Name of the IRIS spatial layer
       * weather_station_file_name : File name of the weather station nearest to each spatial unit
       * width : Width of the spatial unit in meters
       * length : Length of the spatial unit in meters
       * perimeter : Perimeter of the spatial unit in meters
       * area : Area of each spatial unit in m2
       * wkb_geometry : Wkb geometry of each spatial unit
       * tall_ratio : Tall vegetation ratio [%] of each spatial unit
       * short_ratio : Short vegetation ratio [%] of each spatial unit
       * veg_ratio : Total vegetation ratio [%] of each spatial unit
       * rurvegcover : Total vegetation ratio [%] of the spatial unit in which the meteo file is located
       * ver_to_hor : Vertical to horizontal aspect ratio of each spatial unit
       * bld_dens : Building density of each spatial unit [0-1]
       * bld_hgt : Weighted Average building height of each spatial unit

    :Globals:
     - *engine* (:class:`_engine.Engine` instance.): for activating connection to the PostGIS database.
     - *dbname*: (str) name of the database - defaults to `gorenove_fabric`.
     - *user*: (str) username.
     - *host*: (str) database host address - defaults to `localhost`.
     - *password*: (str) password used to authenticate
     - *port*: (str) connection port number - defaults to 35432
     - *active_schema*: (str) connection port number - defaults to `sat4bdnb`

    kwargs: table names to produce / used

        - **[1]** sp_un_table: `sat4bdnb.giris_init`
        - **[2]** iuhi_table: `sat4bdnb.uhi`
        - **[3]** iris_table: `referentiel_administratif_millesime_2023_11.iris`
        - **[4]** weather_ind_table: `sat4bdnb.weather_indicators`
        - **[5]** init_grid_table: `sat4bdnb.tile_grid`
        - **[6]** oso_table: `sat4bdnb.veg_oso_raster_inter_1`
        - **[7]** uasl_table: `sat4bdnb.veg_uasl_inter`

    :return: PostgreSQL table `uwg_spatial_units` in `sat4bdnb` Schema

    """
    # TODO: remove `AND code_giris LIKE '75%'` from `init_sp_query` for national level simulation

    """Create the uwg_spatial_units"""
    init_sp_query = f"""
    DROP TABLE IF EXISTS {sp_un_table} ;
    CREATE table {sp_un_table} as (
    WITH data_uhi AS (
        SELECT code_giris, geometry_giris, iuhi 
        FROM {iuhi_table} 
        WHERE veg_cover < 0.65 
        AND population_density > 1000 
        AND iuhi > 5 
        AND code_giris LIKE '75%'
    ),
    data_iris AS (
        SELECT rr.code_iris, rr.libelle_iris, rr.code_grand_iris, wi.weather_station_file_name, rr.geom_iris
        FROM {iris_table} rr
        JOIN data_uhi 
        ON rr.code_grand_iris = data_uhi.code_giris
        JOIN {weather_ind_table} wi 
        ON wi.code_giris = rr.code_grand_iris
    ),
    data_grid AS (
        SELECT di.code_iris, di.libelle_iris label_iris, di.weather_station_file_name, tg.wkb_geometry
        FROM {init_grid_table} tg 
        JOIN data_iris di
        ON ST_contains(di.geom_iris, ST_Centroid(tg.wkb_geometry))
    )
    SELECT 
        row_number() OVER () AS uwg_sp_un_id, 
        code_iris, 
        label_iris, 
        weather_station_file_name,
        ROUND((MAX(ST_XMax(wkb_geometry)) - MIN(ST_XMin(wkb_geometry)))::numeric, 1) AS width,
        ROUND((MAX(ST_YMax(wkb_geometry)) - MIN(ST_YMin(wkb_geometry)))::numeric, 1) AS length,
        ROUND((st_perimeter(wkb_geometry))::numeric, 1) as perimeter,    
        ROUND((st_area(wkb_geometry))::numeric, 1) as area, 
        wkb_geometry
    FROM 
        data_grid
        group by data_grid.code_iris, data_grid.label_iris, data_grid.weather_station_file_name, data_grid.wkb_geometry
        ); """

    "Create GIST index"
    idx_query = f"""
    ALTER TABLE {sp_un_table} ADD CONSTRAINT uwg_sp_un_id PRIMARY KEY (uwg_sp_un_id);
    ALTER TABLE {sp_un_table} ADD CONSTRAINT uwg_sp_un_id2 FOREIGN KEY (uwg_sp_un_id) REFERENCES {init_grid_table} (ogc_fid);
    CREATE INDEX uwg_sp_un_wkb_geometry_idx ON {sp_un_table} USING gist (wkb_geometry);
    CREATE INDEX uwg_sp_un_code_iris_idx ON {sp_un_table} USING btree (code_iris);
    CREATE INDEX uwg_sp_un_id_idx ON {sp_un_table} USING btree (uwg_sp_un_id);
    CREATE INDEX uwg_sp_un_weather_station_file_name_idx ON {sp_un_table} USING btree (weather_station_file_name);
    CREATE INDEX uwg_sp_un_label_iris_idx ON {sp_un_table} USING btree (label_iris);
    """

    "DO VACUUM"
    vac_querry = f"VACUUM FULL ANALYZE {sp_un_table};"

    "alter table"
    alter_query = f"""
    ALTER TABLE {sp_un_table} ADD COLUMN tall_ratio numeric DEFAULT 0;
    ALTER TABLE {sp_un_table} ADD COLUMN short_ratio numeric DEFAULT 0;
    ALTER TABLE {sp_un_table} ADD COLUMN veg_ratio numeric DEFAULT 0;
    ALTER TABLE {sp_un_table} ADD COLUMN ADD COLUMN ver_to_hor NUMERIC(8, 4) DEFAULT 0,
    ALTER TABLE {sp_un_table} ADD COLUMN ADD COLUMN bld_dens NUMERIC(8, 4) DEFAULT 0,
    ALTER TABLE {sp_un_table} ADD COLUMN ADD COLUMN bld_hgt NUMERIC(8, 4) DEFAULT 0;
    """

    "add vegetation ind"
    update_query = f"""
    update {sp_un_table} 
        set tall_ratio  = veg_data.tall_ratio , 
        short_ratio = veg_data.short_ratio, 
        veg_ratio  = veg_data.veg_ratio 
    from (
        with data_oso as (
            select inn.uwg_sp_un_id,
            case 
                when inn.classe in (16, 17) then 'veg_tall'
                else  'veg_short'
                end as  group_class,
            ST_Union(inn.oso_geom) AS oso_geom_clas
            from (
            select uwg.uwg_sp_un_id, ST_Intersection(vor.oso_rst_inter_one_geometry, uwg.wkb_geometry) AS oso_geom, vor.classe  
            from {sp_un_table} uwg
            left join  {oso_table} vor
            on LEFT(vor.code_giris, 5) = LEFT(uwg.code_iris, 5) and ST_Intersects(vor.oso_rst_inter_one_geometry, uwg.wkb_geometry) 
                ) as  inn
                group by inn.uwg_sp_un_id, group_class
        ),
        data_uasl as (
            select  t1.uwg_sp_un_id, t1.veg_uasl_id, (ST_Dump(t1.uasl_geom)).geom as uasl_geom_dist, 'veg_tall' as  group_class
                from (
            select  
            uwg.uwg_sp_un_id, vui.veg_uasl_id, ST_Intersection(vui.uasl_geometry, uwg.wkb_geometry) as uasl_geom 
            from  {sp_un_table} uwg
            join  {uasl_table} vui
            on  
            LEFT(vui.code_giris, 5) = LEFT(uwg.code_iris, 5) and  ST_Intersects(vui.uasl_geometry, uwg.wkb_geometry) 
                ) as  t1
            left join  
            data_oso dos 
            on  dos.uwg_sp_un_id = t1.uwg_sp_un_id and ST_Intersects(t1.uasl_geom, dos.oso_geom_clas) 
            where  dos.oso_geom_clas is  null 
        ),
        data_combined as (
            select uwg_sp_un_id, oso_geom_clas as combined_geom, group_class as description from data_oso
            union 
            select uwg_sp_un_id, uasl_geom_dist , group_class from data_uasl
        ),
        ratio_data as(
            select  dc.uwg_sp_un_id, dc.combined_geom, dc.description,
            case 
                when dc.description like 'veg_tall' then St_area(combined_geom)/area
                    else 0
            end as tall_ratio,
            case 
                when dc.description like 'veg_short' then St_area(combined_geom)/area
                    else 0
            end as short_ratio
            from data_combined dc, {sp_un_table} uwg
            where dc.uwg_sp_un_id=uwg.uwg_sp_un_id 
        ),
        final_ratio as (
            select uwg_sp_un_id, round(cast(sum(tall_ratio) as numeric),4)*100 tall_ratio, round(cast(sum(short_ratio)as numeric),4)*100 short_ratio
            from ratio_data rd
            group by rd.uwg_sp_un_id
        ),
        data_to_select as (
            select fr.uwg_sp_un_id, fr.tall_ratio , fr.short_ratio, sum(fr.tall_ratio+fr.short_ratio) as veg_ratio
            from final_ratio fr
            group by fr. uwg_sp_un_id, fr.tall_ratio , fr.short_ratio
        ),
        final_table as (
            select  uwg.uwg_sp_un_id, ds.tall_ratio , ds.short_ratio, ds.veg_ratio
            from {sp_un_table} uwg
            left join data_to_select ds
            on uwg.uwg_sp_un_id = ds.uwg_sp_un_id
        ) 
        select uwg.uwg_sp_un_id, 
        coalesce(ds.tall_ratio, 0) AS tall_ratio, 
        coalesce(ds.short_ratio, 0) AS short_ratio, 
        coalesce(ds.veg_ratio, 0) AS veg_ratio
        from {sp_un_table} uwg
        left join data_to_select ds 
        on uwg.uwg_sp_un_id = ds.uwg_sp_un_id
        ) AS veg_data
    where {sp_un_table}.uwg_sp_un_id = veg_data.uwg_sp_un_id;
    """

    "add btree ind"
    update_idx_query = f"""
    CREATE INDEX uwg_sp_un_tall_ratio_idx ON {sp_un_table} USING btree (tall_ratio);
    CREATE INDEX uwg_sp_un_short_ratio_idx ON {sp_un_table} USING btree (short_ratio);
    CREATE INDEX uwg_sp_un_veg_ratio_idx ON {sp_un_table} USING btree (veg_ratio);
    """

    update_rural_veg_cover_query = f"""
    ALTER TABLE {sp_un_table} ADD COLUMN rurvegcover numeric DEFAULT 0;
    update {sp_un_table} 
        set rurvegcover = weather_ind.rural_veg from (
            SELECT usu.*, veg_ratio as rural_veg
            FROM {sp_un_table} usu 
            JOIN sat4bdnb.indicators_per_weather_file ipwf 
            ON ST_Intersects(usu.wkb_geometry, ipwf.geometry)
            )
            as weather_ind;
    """

    update_aged_query = f"""
    ALTER TABLE {sp_un_table} ADD COLUMN aged_ratio numeric DEFAULT 0;
    update {sp_un_table} 
    set aged_ratio = pop.aged_ratio from (
        select  
        usu.uwg_sp_un_id, 
        usu.wkb_geometry, 
        case  
            when  SUM(fil.ind_65_79 + fil.ind_80p) = 0 or ind = 0 or ind is null  then 0 -- Handle division by zero
            else  round(SUM(fil.ind_65_79 + fil.ind_80p) / fil.ind, 2)
            end as aged_ratio
        from  
            {sp_un_table} usu
        left join  
            {pop_table} fil 
        on  left(usu.code_iris, 5) = fil.lcog_geo and st_intersects(usu.wkb_geometry, st_centroid(fil.wkb_geometry))
        group  by  
            usu.uwg_sp_un_id, 
            usu.wkb_geometry, 
            fil.ind)
        as pop;
    """

    "execute"
    exec_sqlalchemy_query(engine, PG_ROLE, init_sp_query)
    print('1')
    exec_sqlalchemy_query(engine, PG_ROLE, idx_query)
    exec_sqlalchemy_query(engine, PG_ROLE, vac_querry, type='vacuum')
    print('2')
    exec_sqlalchemy_query(engine, PG_ROLE, alter_query)
    print('3')
    exec_sqlalchemy_query(engine, PG_ROLE, update_query)
    print('4')
    exec_sqlalchemy_query(engine, PG_ROLE, update_idx_query)
    print('5')
    exec_sqlalchemy_query(engine, PG_ROLE, update_rural_veg_cover_query)
    exec_sqlalchemy_query(engine, PG_ROLE, vac_querry, type='vacuum')
    print('6')
    exec_sqlalchemy_query(engine, PG_ROLE, update_aged_query)
    exec_sqlalchemy_query(engine, PG_ROLE, vac_querry, type='vacuum')

    "Document Table"

    q_table_comment = f"""
           COMMENT ON TABLE {sp_un_table} IS 'Spatial calculation domain of the UWG simulations.';
           COMMENT ON COLUMN {sp_un_table}.uwg_sp_un_id IS 'Identifier of each spatial layer';
           COMMENT ON COLUMN {sp_un_table}.code_iris IS 'Unique Identifier of each IRIS spatial layer that intersects with the uwg spatial unit';
           COMMENT ON COLUMN {sp_un_table}.label_iris IS 'Name of the IRIS spatial layer';
           COMMENT ON COLUMN {sp_un_table}.weather_station_file_name IS 'File name of the weather station nearest to each spatial unit';
           COMMENT ON COLUMN {sp_un_table}.width IS 'Width of the spatial unit in meters';
           COMMENT ON COLUMN {sp_un_table}.length IS 'Length of the spatial unit in meters';
           COMMENT ON COLUMN {sp_un_table}.perimeter IS 'Perimeter of the spatial unit in meters';
           COMMENT ON COLUMN {sp_un_table}.area IS 'Area of each spatial unit in m2';
           COMMENT ON COLUMN {sp_un_table}.wkb_geometry IS 'Wkb geometry of each spatial unit;
           COMMENT ON COLUMN {sp_un_table}.tall_ratio IS 'Tall vegetation ratio [%] of each spatial unit';
           COMMENT ON COLUMN {sp_un_table}.short_ratio IS 'Short vegetation ratio [%] of each spatial unit';
           COMMENT ON COLUMN {sp_un_table}.veg_ratio IS 'Total vegetation ratio [%] of each spatial unit';
           COMMENT ON COLUMN {sp_un_table}.rurvegcover IS 'Total vegetation ratio [%] of the spatial unit in which the meteo file is located';
           COMMENT ON COLUMN {sp_un_table}.ver_to_hor IS 'Vertical to horizontal aspect ratio of each spatial unit';
           COMMENT ON COLUMN {sp_un_table}.bld_dens IS 'Building density of each spatial unit [0-1]';
           COMMENT ON COLUMN {sp_un_table}.bld_hgt IS 'Weighted Average building height of each spatial unit';
           -- COMMENT ON COLUMN {sp_un_table}.aged_ratio IS 'Percentage of aged people of each spatial unit';

           """
    exec_sqlalchemy_query(engine, PG_ROLE, q_table_comment)

    "Alter ownership of table"

    own_query = f"""alter table {sp_un_table} OWNER TO {active_schema};"""
    exec_sqlalchemy_query(engine, PG_ROLE, own_query)

@timeit
def request_spatial_units(**kwargs):
    from shapely import wkb

    init_sp_query = f"""
        SELECT 
            uwg_sp_un_id,
            code_iris,
            wkb_geometry,
            area,
            length,
            width,
            perimeter,
            tall_ratio,
            short_ratio,
            rurvegcover,
            weather_station_file_name
        from {sp_un_table}; """

    districts = {n: {'sp_id': d[0],
                     'code_iris': d[1],
                     'geometry': wkb.loads(bytes.fromhex(d[2])),
                     'surface': float(d[3]),
                     'length': float(d[4]),
                     'width': float(d[5]),
                     'perimeter': float(d[6]),
                     'tall_ratio': float(d[7]),
                     'short_ratio': float(d[8]),
                     'rurvegcover': float(d[9]),
                     'weather_station_file_name': d[10]
                     }
                 for n, d in enumerate(exec_sqlalchemy_query(engine, PG_ROLE, init_sp_query, type='fetchall'))
                 }

    gdf_dis = query_dict_to_gpd(districts, sort='sp_id')

    assert not gdf_dis.empty, "No spatial units found. Cannot proceed."

    return gdf_dis


@timeit
def request_bld_data_ver_to_hor(sp_id: str, **kwargs):

    bld_query = f"""
    WITH bld_data AS (
        SELECT 
            usu.uwg_sp_un_id, 
    --        bdnb.materiaux_toiture_simplifie, 
            bdnb.batiment_groupe_id, 
            bdnb.hauteur, 
            ST_Multi(ST_Intersection(usu.wkb_geometry, bdnb.geom_cstr)) AS bld_geom
        FROM 
            (
                SELECT *
                FROM {sp_un_table}
                WHERE uwg_sp_un_id = {sp_id}
            ) usu 
        JOIN (
            SELECT 
                bc.geom_cstr, bc.batiment_groupe_id, bc.hauteur, bc.code_iris
    --            , 
    --            bb.materiaux_toiture_simplifie
            FROM 
                {table_bat_cstr} bc
            JOIN 
                {table_bat_syn_env} bb
            ON
                bc.batiment_groupe_id = bb.batiment_groupe_id
            WHERE
                bc.fictive_geom_cstr = False
        ) bdnb
        ON
            left(usu.code_iris, 7) = left(bdnb.code_iris, 7) 
            AND ST_Intersects(usu.wkb_geometry, bdnb.geom_cstr)
    )
    SELECT 
        uwg_sp_un_id,
    --    materiaux_toiture_simplifie,
        batiment_groupe_id,
        hauteur,
        bld_geom
    FROM 
        bld_data 
    WHERE 
        bld_geom IS NOT NULL;
    
    """
    blds = {n: {'bld_id': d[1],
                'height': float(d[2]),
                'geometry': wkb.loads(bytes.fromhex(d[3])),
                }
            for n, d in enumerate(exec_sqlalchemy_query(engine, PG_ROLE, bld_query, type='fetchall'))
            }

    gdf_blds = query_dict_to_gpd(blds, sort='bld_id')

    return gdf_blds


def request_bld_data_bdtopo(sp_id: str, **kwargs):

    bld_query = f"""
    WITH bld_data AS (
        SELECT 
            usu.uwg_sp_un_id, 
            bdnb.batiment_id, 
            bdnb.hauteur, 
            ST_Multi(ST_Intersection(usu.wkb_geometry, bdnb.geometrie)) AS bld_geom
        FROM 
            (
                SELECT *
                FROM {sp_un_table}
                WHERE uwg_sp_un_id = {sp_id}
            ) usu 
        JOIN (
            SELECT 
                bc.geometrie, bc.cleabs as batiment_id, bc.hauteur, bc.departement
            FROM 
                {table_bat_topo} bc
        ) bdnb
        ON
            left(usu.code_iris, 2) = departement 
            AND ST_Intersects(usu.wkb_geometry, bdnb.geometrie)
    )
    SELECT 
        uwg_sp_un_id,
        batiment_id,
        hauteur,
        bld_geom
    FROM 
        bld_data 
    WHERE 
        bld_geom IS NOT NULL;

    """
    blds = {n: {'bld_id': d[1],
                'height': float(d[2]) if d[2] is not None else None ,
                'geometry': wkb.loads(bytes.fromhex(d[3])),
                }
            for n, d in enumerate(exec_sqlalchemy_query(engine, PG_ROLE, bld_query, type='fetchall'))
            }

    gdf_blds = query_dict_to_gpd(blds, sort='bld_id')

    return gdf_blds

@timeit
def request_bld_data_classification(sp_id: str, **kwargs):

    bld_query = f"""
      WITH bdnb_data AS (
    SELECT 
        usu.uwg_sp_un_id,
        batiment_groupe_id,
        hauteur,
        ST_Multi(ST_Intersection(usu.wkb_geometry, bdnb.geom_cstr)) AS bld_geom, 
     bdnb.code_iris,
bdnb.code_departement_insee, 
bdnb.materiaux_toiture_simplifie, bdnb.materiaux_structure_mur_exterieur_simplifie, 
        type_dpe,
        type_batiment_dpe,
        periode_construction_dpe,
        annee_construction_dpe, 
        nombre_niveau_logement,
        nombre_niveau_immeuble, 
        surface_habitable_immeuble,
        surface_habitable_logement,
        conso_5_usages_ep_m2,
        conso_5_usages_ef_m2,
        emission_ges_5_usages_m2,
        classe_bilan_dpe,
        classe_emission_ges,
        classe_conso_energie_arrete_2012,
        classe_emission_ges_arrete_2012,
        conso_3_usages_ep_m2_arrete_2012,
        emission_ges_3_usages_ep_m2_arrete_2012,
        type_installation_chauffage,
        type_energie_chauffage,
        type_generateur_chauffage,
        nb_generateur_chauffage,
        nb_installation_chauffage,
        type_energie_climatisation,
        type_generateur_climatisation,
        type_installation_ecs,
        type_energie_ecs,
        type_generateur_ecs,
        type_generateur_ecs_anciennete,
        nb_generateur_ecs,
        nb_installation_ecs,
        type_ventilation,
        type_vitrage,
        type_materiaux_menuiserie,
        type_gaz_lame,
        epaisseur_lame,
        surface_vitree_nord,
        surface_vitree_sud,
        surface_vitree_ouest,
        surface_vitree_est,
        surface_vitree_horizontal,
        u_baie_vitree,
        uw,
        facteur_solaire_baie_vitree,
        pourcentage_surface_baie_vitree_exterieur,
        type_isolation_mur_exterieur,
        epaisseur_isolation_mur_exterieur_estim,
        materiaux_structure_mur_exterieur,
        epaisseur_structure_mur_exterieur,
        surface_mur_totale,
        surface_mur_exterieur,
        surface_mur_deperditif,
        u_mur_exterieur,
        l_local_non_chauffe_mur,
        type_isolation_plancher_bas,
        type_plancher_bas_deperditif,
        surface_plancher_bas_totale,
        surface_plancher_bas_deperditif,
        u_plancher_bas_final_deperditif,
        u_plancher_bas_brut_deperditif,
        type_isolation_plancher_haut,	
        type_plancher_haut_deperditif,
        surface_plancher_haut_totale,
        surface_plancher_haut_deperditif,
        u_plancher_haut_deperditif,
        classe_inertie,
        deperdition_mur,
        deperdition_baie_vitree,
        deperdition_plancher_bas,
        deperdition_plancher_haut,
        deperdition_pont_thermique,
        deperdition_porte
    FROM 
        (
            SELECT *
            FROM {sp_un_table}
            WHERE uwg_sp_un_id = {sp_id}
        ) usu 
    JOIN (
        SELECT 
            bc.geom_cstr, bc.batiment_groupe_id, bc.hauteur, bc.code_iris,
        bc.code_departement_insee,  
        bgse.materiaux_toiture_simplifie, bgse.materiaux_structure_mur_exterieur_simplifie,
            type_dpe,
        bgdr.type_batiment_dpe,
            periode_construction_dpe,
            annee_construction_dpe,
            nombre_niveau_logement,
            nombre_niveau_immeuble,
            surface_habitable_immeuble,
            surface_habitable_logement,
            conso_5_usages_ep_m2,
            conso_5_usages_ef_m2,
            emission_ges_5_usages_m2,
            classe_bilan_dpe,
            classe_emission_ges,
            classe_conso_energie_arrete_2012,
            classe_emission_ges_arrete_2012,
            conso_3_usages_ep_m2_arrete_2012,
            emission_ges_3_usages_ep_m2_arrete_2012,
            type_installation_chauffage,
            type_energie_chauffage,
            type_generateur_chauffage,
            nb_generateur_chauffage,
            nb_installation_chauffage,
            type_energie_climatisation,
            type_generateur_climatisation,
            type_installation_ecs,
            type_energie_ecs,
            type_generateur_ecs,
            type_generateur_ecs_anciennete,
            nb_generateur_ecs,
            nb_installation_ecs,
            type_ventilation,	
        bgdr.type_vitrage,
        bgdr.type_materiaux_menuiserie,
        bgdr.type_gaz_lame,
        bgdr.epaisseur_lame,
        bgdr.surface_vitree_nord,
        surface_vitree_sud,
        surface_vitree_ouest,
        surface_vitree_est,
        surface_vitree_horizontal,
        bgdr.u_baie_vitree,
        bgdr.uw,
        bgdr.facteur_solaire_baie_vitree,
        bgdr.pourcentage_surface_baie_vitree_exterieur,
        bgdr.type_isolation_mur_exterieur,
        bgdr.epaisseur_isolation_mur_exterieur_estim,
        bgdr.materiaux_structure_mur_exterieur,
        bgdr.epaisseur_structure_mur_exterieur,
            surface_mur_totale,
            surface_mur_exterieur,
            surface_mur_deperditif,
        bgdr.u_mur_exterieur,
        bgdr.l_local_non_chauffe_mur,
        bgdr.type_isolation_plancher_bas,
        bgdr.type_plancher_bas_deperditif,
            surface_plancher_bas_totale,
            surface_plancher_bas_deperditif,
        bgdr.u_plancher_bas_final_deperditif,
        bgdr.u_plancher_bas_brut_deperditif,
        bgdr.type_isolation_plancher_haut,	
        bgdr.type_plancher_haut_deperditif,
            surface_plancher_haut_totale,
            surface_plancher_haut_deperditif,
        bgdr.u_plancher_haut_deperditif,
        bgdr.classe_inertie,	
            deperdition_mur,
            deperdition_baie_vitree,
            deperdition_plancher_bas,
            deperdition_plancher_haut,
            deperdition_pont_thermique,
            deperdition_porte
--            , 
--            bb.materiaux_toiture_simplifie
        FROM 
            {table_bat_cstr} bc
        join 
{table_bat_syn_env} bgse
on
bc.batiment_groupe_id =bgse.batiment_groupe_id
        join 
        	{table_bat_dpe} bgdr
        ON
            bc.batiment_groupe_id = bgdr.batiment_groupe_id

        WHERE
            bc.fictive_geom_cstr = false --and type_dpe like 'dpe arrêté 2021 3cl logement'
    ) bdnb
    ON
        left(usu.code_iris, 7) = left(bdnb.code_iris, 7) 
        AND ST_Intersects(usu.wkb_geometry, bdnb.geom_cstr)
)
SELECT 
    bdnb_data.*
FROM 
    bdnb_data 
WHERE 
    bld_geom IS NOT NULL;
    """

    d = exec_sqlalchemy_query(engine, PG_ROLE, bld_query, type='fetchall')

    col = ['uwg_sp_un_id,batiment_groupe_id,hauteur,bld_geom, code_iris,'
           'code_departement_insee,materiaux_toiture_simplifie,materiaux_structure_mur_exterieur_simplifie,type_dpe,'
           'type_batiment_dpe,periode_construction_dpe,annee_construction_dpe,nombre_niveau_logement,'
           'nombre_niveau_immeuble,surface_habitable_immeuble,surface_habitable_logement,conso_5_usages_ep_m2,'
           'conso_5_usages_ef_m2,emission_ges_5_usages_m2,classe_bilan_dpe,classe_emission_ges,'
           'classe_conso_energie_arrete_2012,classe_emission_ges_arrete_2012,conso_3_usages_ep_m2_arrete_2012,'
           'emission_ges_3_usages_ep_m2_arrete_2012,type_installation_chauffage,type_energie_chauffage,'
           'type_generateur_chauffage,nb_generateur_chauffage,nb_installation_chauffage,type_energie_climatisation,'
           'type_generateur_climatisation,type_installation_ecs,type_energie_ecs,type_generateur_ecs,'
           'type_generateur_ecs_anciennete,nb_generateur_ecs,nb_installation_ecs,type_ventilation,type_vitrage,'
           'type_materiaux_menuiserie,type_gaz_lame,epaisseur_lame,surface_vitree_nord,surface_vitree_sud,'
           'surface_vitree_ouest,surface_vitree_est,surface_vitree_horizontal,u_baie_vitree,uw,'
           'facteur_solaire_baie_vitree,pourcentage_surface_baie_vitree_exterieur,type_isolation_mur_exterieur,'
           'epaisseur_isolation_mur_exterieur_estim,materiaux_structure_mur_exterieur,'
           'epaisseur_structure_mur_exterieur,surface_mur_totale,surface_mur_exterieur,surface_mur_deperditif,'
           'u_mur_exterieur,l_local_non_chauffe_mur,type_isolation_plancher_bas,type_plancher_bas_deperditif,'
           'surface_plancher_bas_totale,surface_plancher_bas_deperditif,u_plancher_bas_final_deperditif,'
           'u_plancher_bas_brut_deperditif,type_isolation_plancher_haut,type_plancher_haut_deperditif,'
           'surface_plancher_haut_totale,surface_plancher_haut_deperditif,u_plancher_haut_deperditif,classe_inertie,'
           'deperdition_mur,deperdition_baie_vitree,deperdition_plancher_bas,deperdition_plancher_haut,'
           'deperdition_pont_thermique,deperdition_porte']

    # blds = {n: {
    #     'bld_id': d[1],
    #     'height': float(d[2]) if d[2] is not None else None,
    #     'geometry': wkb.loads(bytes.fromhex(d[3])),
    #     'type_dpe': d[4],
    #     'type_batiment_dpe': d[5],
    #     'periode_construction_dpe': d[6],
    #     'annee_construction_dpe': int(d[7]) if d[7] is not None else None,
    #     'nombre_niveau_logement': int(d[8]) if d[8] is not None else None,
    #     'nombre_niveau_immeuble': int(d[9]) if d[9] is not None else None,
    #     'surface_habitable_immeuble': float(d[10]) if d[10] is not None else None,
    #     'surface_habitable_logement': float(d[11]) if d[11] is not None else None,
    #     'conso_5_usages_ep_m2': float(d[12]) if d[12] is not None else None,
    #     'conso_5_usages_ef_m2': float(d[13]) if d[13] is not None else None,
    #     'emission_ges_5_usages_m2': float(d[14]) if d[14] is not None else None,
    #     'classe_bilan_dpe': d[15],
    #     'classe_emission_ges': d[16],
    #     'classe_conso_energie_arrete_2012': d[17],
    #     'classe_emission_ges_arrete_2012': d[18],
    #     'conso_3_usages_ep_m2_arrete_2012': d[19],
    #     'emission_ges_3_usages_ep_m2_arrete_2012': d[20],
    #     'type_installation_chauffage': d[21],
    #     'type_energie_chauffage': d[22],
    #     'type_generateur_chauffage': d[23],
    #     'nb_generateur_chauffage': int(d[24]) if d[24] is not None else None,
    #     'nb_installation_chauffage': int(d[25]) if d[25] is not None else None,
    #     'type_energie_climatisation': d[26],
    #     'type_generateur_climatisation': d[27],
    #     'type_installation_ecs': d[28],
    #     'type_energie_ecs': d[29],
    #     'type_generateur_ecs': d[30],
    #     'type_generateur_ecs_anciennete': d[31],
    #     'nb_generateur_ecs': int(d[32]) if d[32] is not None else None,
    #     'nb_installation_ecs': int(d[33]) if d[33] is not None else None,
    #     'type_ventilation': d[34],
    #     ' type_vitrage': d[35],
    #     'type_materiaux_menuiserie': d[36],
    #     'type_gaz_lame': d[37],
    #     'epaisseur_lame': int(d[38]) if d[38] is not None else None,
    #     'surface_vitree_nord': float(d[39]) if d[39] is not None else None,
    #     'surface_vitree_sud': float(d[40]) if d[40] is not None else None,
    #     'surface_vitree_ouest': float(d[41]) if d[41] is not None else None,
    #     'surface_vitree_est': float(d[42]) if d[42] is not None else None,
    #     'surface_vitree_horizontal': float(d[43]) if d[43] is not None else None,
    #     'u_baie_vitree': float(d[44]) if d[44] is not None else None,
    #     'uw': float(d[45]) if d[45] is not None else None,
    #     'facteur_solaire_baie_vitree': float(d[46]) if d[46] is not None else None,
    #     'pourcentage_surface_baie_vitree_exterieur': float(d[47]) if d[47] is not None else None,
    #     'type_isolation_mur_exterieur': d[48],
    #     'epaisseur_isolation_mur_exterieur_estim': float(d[49]) if d[49] is not None else None,
    #     'materiaux_structure_mur_exterieur': d[50],
    #     'epaisseur_structure_mur_exterieur': d[51],
    #     'surface_mur_totale': float(d[52]) if d[52] is not None else None,
    #     'surface_mur_exterieur': float(d[53]) if d[53] is not None else None,
    #     'surface_mur_deperditif': float(d[54]) if d[54] is not None else None,
    #     'u_mur_exterieur': float(d[55]) if d[55] is not None else None,
    #     'type_isolation_plancher_bas': d[57],
    #     'type_plancher_bas_deperditif': d[58],
    #     'surface_plancher_bas_totale': float(d[59]) if d[59] is not None else None,
    #     'surface_plancher_bas_deperditif': (d[60]),
    #     'u_plancher_bas_final_deperditif': float(d[61]) if d[61] is not None else None,
    #     'u_plancher_bas_brut_deperditif': float(d[62]) if d[62] is not None else None,
    #     'type_isolation_plancher_haut': d[63],
    #     'type_plancher_haut_deperditif': d[64],
    #     'surface_plancher_haut_totale': float(d[65]) if d[65] is not None else None,
    #     'surface_plancher_haut_deperditif': float(d[66]) if d[66] is not None else None,
    #     'u_plancher_haut_deperditif': float(d[67]) if d[67] is not None else None,
    #     'classe_inertie': d[68],
    #     'deperdition_mur': float(d[69]) if d[69] is not None else None,
    #     'deperdition_baie_vitree': float(d[70]) if d[70] is not None else None,
    #     'deperdition_plancher_bas': float(d[71]) if d[71] is not None else None,
    #     'deperdition_plancher_haut': float(d[72]) if d[72] is not None else None,
    #     'deperdition_pont_thermique': float(d[73]) if d[73] is not None else None,
    #     'deperdition_porte': float(d[74]) if d[74] is not None else None
    # }
    #     for n, d in enumerate(exec_sqlalchemy_query(engine, PG_ROLE, bld_query, type='fetchall'))
    # }

    columns = [s.strip() for s in col[0].split(',')]
    geom = [wkb.loads(bytes.fromhex(k[3])) for k in d]
    df = gpd.GeoDataFrame(d, columns=columns, geometry=geom, crs='EPSG:2154')

    gdf_blds = df.apply(convert_decimal_to_float)

    return gdf_blds


def insert_values_to_col(columns_dic, **kwargs):
    target_table = kwargs.get('table_name')
    id_column = kwargs.get('id_col')
    id = kwargs.get('id')

    set_clause = ", ".join([f"{key} = {value}" for key, value in columns_dic.items()])
    # Construct the SQL UPDATE statement
    q = f"""
        UPDATE {target_table}
        SET {set_clause}
        WHERE {id_column} = {id};
    """

    exec_sqlalchemy_query(engine, PG_ROLE, q)






# uwg_sp_units.to_file(r'D:\YORGOS\Work\Projects\Renoptim\with_bld.gpkg', driver='GPKG')

# classification of bld based on
# `dpe arrêté 2021 3cl logement`

# table bdnb
# batiment_groupe_dpe_representatif_logement
