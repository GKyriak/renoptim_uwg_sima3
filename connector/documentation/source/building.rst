Building data
--------------

Building data correspond to `BDNBv07` database.

The following tables are used (see: `ci/bdnb_tables.yml`) to extract:
    * Building footprints,
    * Envelope characteristics, and
    * DPE class

1. batiment_construction
2. batiment_groupe_dpe_representatif_logement
3. batiment_groupe_synthese_enveloppe

enhanced by :

4. rerentiel_administratif_millesime_2023_11.iris

for the iris description.
