T_INIT = 293


class Element(object):
    """
   An Element object in UWG is defined by the following parameters:
       * type (str)
       * albedo (float)
       * emissivity (float)
       * layer_thickness_lst (list)
       * material_lst (list)
       * vegcoverage (float)
       * t_init (float)
       * horizontal (boolean)
       * name (str)
       """
    def __init__(self, albedo, emissivity, layer_thickness_lst, material_lst, vegcoverage,
                 horizontal, name):
        self.type = 'Element'
        self.albedo = albedo
        self.emissivity = emissivity
        self.layer_thickness_lst = layer_thickness_lst
        self.material_lst = material_lst
        self.vegcoverage = vegcoverage
        self.t_init = T_INIT
        self.horizontal = horizontal
        self.name = name


class Material(object):
    """
    A Material object in UWG is defined by the following parameters:
       * type (str)
       * name (str)
       * thermalcond (float)
       * volheat (float)
       """
    def __init__(self, name, thermalcond, volheat):
        self.type = 'Material'
        self.name = name
        self.thermalcond = thermalcond
        self.volheat = volheat
