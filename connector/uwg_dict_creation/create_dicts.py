from connector.classes.building_object import Building, BuildingObject
from connector.classes.element_object import Element
from connector.utils.materials import thermal_mat_obj, PRECAT_ARDOISE, PRECAT_ZINC, PRECAT_TUILE


def create_representative_building(flr_hgt: float, glazing_ratio: float, u_value: float, shgc: float,
                                   alb_wall: float, emis_wall: float, alb_roof: float, emis_roof: float, alb_fl: float,
                                   emis_fl: float, vegcoverage: float,
                                   layer_thickness_lst_wall: list, material_lst_wall: list,
                                   layer_thickness_lst_roof: list, material_lst_roof: list,
                                   layer_thickness_lst_floor: list, material_lst_floor: list,
                                   builtera: str, name: str, condtype: str
                                   ):
    """

    :param flr_hgt: weighted floor height / DPE class / departement
    :param glazing_ratio: weighted glazing_ratio / DPE class / departement
    :param u_value: weighted glazing_ratio / DPE class / departement
    :param shgc: weighted solar heat gain coef / DPE class / departement
    :param alb_wall: albedo / wall material/ DPE class / departement
    :param emis_wall: emissivity / wall material / DPE class / departement
    :param alb_roof: albedo / roof material / DPE class / departement
    :param emis_roof: emissivity / roof material / DPE class / departement
    :param alb_fl: albedo of mass layer default = 0.2
    :param emis_fl: emissivity of mass layer default = 0.9
    :param vegcoverage: green roof coverage default = 0.
    :param layer_thickness_lst_wall: with wall material thicknesses
    :param material_lst_wall: with wall material types
    :param layer_thickness_lst_roof: with roof material types
    :param material_lst_roof: with roof material types
    :param layer_thickness_lst_floor: with floor material types default = concrete
    :param material_lst_floor: with floor material types default = 0.002
    :param builtera: representing the construction period
    :param name: ??
    :param condtype: generator type, options: AIR - WATER HP

    :return: (dict) A building object required for UWG simulation  / DPE class / departement
    """

    wall = Element(alb_wall, emis_wall, layer_thickness_lst_wall, material_lst_wall, vegcoverage,
                   horizontal=False, name='MassWall')
    roof = Element(alb_roof, emis_roof, layer_thickness_lst_roof, material_lst_roof, vegcoverage,
                   horizontal=True, name='MassRoof')
    mass = Element(alb_fl, emis_fl, layer_thickness_lst_floor, material_lst_floor, vegcoverage,
                   horizontal=True, name='MassFloor')

    wall_dict = wall.__dict__
    roof_dict = roof.__dict__
    mass_dict = mass.__dict__

    building = Building(flr_hgt, glazing_ratio, u_value, shgc, condtype)

    building_dict = building.__dict__

    bld_obj = BuildingObject(building_dict, wall_dict, mass_dict, roof_dict, name, builtera)
    bld_obj_dict = bld_obj.__dict__

    return bld_obj_dict


def generate_material_layer_list(uv: float, wall_dict: dict, elem_type=None):
    """

    :param uv: envelope U-value
    :param wall_dict: A dictionary with building related parameters
    :param elem_type: (str) walll, roof. floor

    :return: (list) of Element materials & thickness
    """

    tolerance = 0.3
    iteration_ftr = 0
    max_iter = 100
    thckns_lst = [0] * 6
    mat_lst = [0] * 6

    mass_idx = (None, None)
    ins_idx = (None, None)

    if elem_type == 'floor':
        mat_lst[0] = mat_lst[-1] = thermal_mat_obj['plaster']['low'].__dict__
        thckns_lst[0] = thckns_lst[-1] = 0.002
        thckns_lst[1] = thckns_lst[2] = thckns_lst[3] = thckns_lst[4] = 0.1 / 4
        mat_lst[1] = mat_lst[2] = mat_lst[3] = mat_lst[4] = thermal_mat_obj['beton']['low'].__dict__

        return thckns_lst, mat_lst

    inertia_lv = {'Légère': 'low', 'Moyenne': 'inter', 'Lourde': 'heavy', 'Très Lourde': 'v_heavy'}[
        wall_dict['inertia']]
    mat_lst[0] = mat_lst[-1] = thermal_mat_obj['plaster'][inertia_lv].__dict__

    insl_pos = wall_dict['insulation_type_wall']
    mass_idx = (3, 4) if 'ITE' in insl_pos else (1, 2)

    if insl_pos == 'non isole':
        mass_idx = (1, 2, 3, 4)
    else:
        ins_idx = (3, 4)

    material_type = wall_dict['wall_material']

    for idx in mass_idx:
        mat_lst[idx] = thermal_mat_obj[material_type][inertia_lv].__dict__
    for idx in ins_idx:
        if idx is not None:
            mat_lst[idx] = thermal_mat_obj['insulation_wall'][inertia_lv].__dict__

    R, plaster_thck, wall_thck, ins_thck = thermal_resistance_calc(inertia_lv, iteration_ftr,
                                                                   wall_dict['wall_thickness'],
                                                                   wall_dict['insul_thickness'],
                                                                   material_type,
                                                                   elem_type)
    Uest = 1 / R

    if elem_type == 'wall':
        nb_iterations = 0
        while abs(uv - Uest) > tolerance and nb_iterations <= max_iter:
            nb_iterations += 1
            iteration_ftr += 1 if uv - Uest > 0 else -1

            R, plaster_thck, wall_thck, ins_thck = thermal_resistance_calc(inertia_lv, iteration_ftr,
                                                                           wall_dict['wall_thickness'],
                                                                           wall_dict['insul_thickness'],
                                                                           material_type,
                                                                           elem_type)

            Uest = 1 / R
        else:
            iteration_ftr = 0
            nb_iterations = 0
            while abs(uv - Uest) > tolerance and nb_iterations <= max_iter:
                iteration_ftr += 0.5 if uv - Uest > 0 else -1
                nb_iterations += 1
                R, plaster_thck, wall_thck, ins_thck = thermal_resistance_calc_forced(inertia_lv, iteration_ftr,
                                                                                      wall_dict['wall_thickness'],
                                                                                      material_type,
                                                                                      )

                Uest = 1 / R

            if abs(uv - Uest) > tolerance:
                raise ValueError('The Solution is not converged for facade thermal properties.')
    thckns_lst[0] = thckns_lst[-1] = plaster_thck

    for idx in mass_idx:
        thckns_lst[idx] = wall_thck / len(mass_idx)

    for idx in ins_idx:
        if idx is not None:
            thckns_lst[idx] = ins_thck / len(ins_idx)

    return thckns_lst, mat_lst


def generate_material_layer_list_roof(uv: float, wall_dict: dict, elem_type=None):
    """

    :param uv:
    :param wall_dict:
    :param elem_type:
    :return:
    """

    if wall_dict['roof_mat'] == 'ardoise':
        return PRECAT_ARDOISE['thckns_lst'], PRECAT_ARDOISE['mat_lst']

    if wall_dict['roof_mat'] == 'zinc-aluminium':
        return PRECAT_ZINC['thckns_lst'], PRECAT_ZINC['mat_lst']

    if wall_dict['roof_mat'] == 'tuile':
        return PRECAT_TUILE['thckns_lst'], PRECAT_TUILE['mat_lst']

    tolerance = 0.3
    iteration_ftr = 0
    max_iter = 100
    thckns_lst = [0] * 6
    mat_lst = [0] * 6

    mass_idx = (None, None)
    ins_idx = (None, None)

    inertia_lv = {'Légère': 'low', 'Moyenne': 'inter', 'Lourde': 'heavy', 'Très Lourde': 'v_heavy'}[
        wall_dict['inertia']]
    mat_lst[0] = mat_lst[-1] = thermal_mat_obj['plaster'][inertia_lv].__dict__

    insl_pos = wall_dict['insulation_type_roof']
    mass_idx = (3, 4) if 'ITE' in insl_pos else (1, 2)

    if insl_pos == 'non isole':
        mass_idx = (1, 2, 3, 4)
    else:
        ins_idx = (3, 4)

    material_type = wall_dict['roof_mat']

    for idx in mass_idx:
        mat_lst[idx] = thermal_mat_obj[material_type][inertia_lv].__dict__
    for idx in ins_idx:
        if idx is not None:
            mat_lst[idx] = thermal_mat_obj['insulation_roof'][inertia_lv].__dict__

    R, plaster_thck, wall_thck, ins_thck = thermal_resistance_calc(inertia_lv, iteration_ftr,
                                                                   wall_dict['wall_thickness'],
                                                                   wall_dict['insul_thickness'],
                                                                   material_type,
                                                                   elem_type)
    Uest = 1 / R

    nb_iterations = 0
    while abs(uv - Uest) > tolerance and nb_iterations <= max_iter:
        nb_iterations += 1
        iteration_ftr += 1 if uv - Uest > 0 else -1

        R, plaster_thck, wall_thck, ins_thck = thermal_resistance_calc(inertia_lv, iteration_ftr,
                                                                       wall_dict['wall_thickness'],
                                                                       wall_dict['insul_thickness'],
                                                                       material_type,
                                                                       elem_type)

        Uest = 1 / R
    else:
        iteration_ftr = 0
        nb_iterations = 0
        while abs(uv - Uest) > tolerance and nb_iterations <= max_iter:
            iteration_ftr += 1 if uv - Uest > 0 else -1
            R, plaster_thck, wall_thck, ins_thck = thermal_resistance_calc_forced(inertia_lv, iteration_ftr,
                                                                                  wall_dict['wall_thickness'],
                                                                                  material_type,
                                                                                  )

            Uest = 1 / R

        if abs(uv - Uest) > tolerance:
            raise ValueError('The Solution is not converged for facade thermal properties.')
    thckns_lst[0] = thckns_lst[-1] = plaster_thck

    for idx in mass_idx:
        thckns_lst[idx] = wall_thck / len(mass_idx)

    for idx in ins_idx:
        if idx is not None:
            thckns_lst[idx] = ins_thck / len(ins_idx)

    return thckns_lst, mat_lst


def thermal_resistance_calc(inertia_lv: str, iterator_factor: float, wall_init_thck: float, ins_init_thck: float,
                            material_type: str, elem_type=None):
    """

    :param inertia_lv: description of inertia level
    :param iterator_factor: number used for iteration purposes
    :param wall_init_thck: initial wall thickness retrieved from bdnb
    :param ins_init_thck: initial insulation thickness retrieved from bdnb
    :param material_type: description of material type
    :param elem_type: wall, roof, floor default -- None
    :return:
    """
    plaster_thck = 0.002
    plaster_cond = thermal_mat_obj['plaster'][inertia_lv].thermalcond

    term1 = plaster_thck / plaster_cond

    wall_thck = wall_init_thck - iterator_factor if \
        ins_init_thck == 0. or ins_init_thck - iterator_factor == 0 else \
        wall_init_thck - ins_init_thck if (wall_init_thck - ins_init_thck) > 0 else wall_init_thck

    wall_cond = thermal_mat_obj[material_type][inertia_lv].thermalcond

    ins_thck = ins_init_thck - iterator_factor if (ins_init_thck - iterator_factor) > 0 else 0.
    ins_cond = thermal_mat_obj['insulation_wall' if elem_type == 'wall' else 'insulation_roof'][inertia_lv].thermalcond

    term2 = wall_thck * 0.01 / wall_cond
    term3 = ins_thck * 0.01 / ins_cond

    R = 2 * (term1 + term2 + term3)

    if ins_thck < 0:
        raise ValueError("Fictive insulation layer has a non-positive thickness.")

    if wall_thck <= 0:
        raise ValueError("Fictive wall layer has a non-positive thickness.")

    return R, plaster_thck, wall_thck * 0.01, ins_thck * 0.01


def thermal_resistance_calc_forced(inertia_lv: str, iterator_factor: float, wall_init_thck: float,
                                   material_type: str):
    """

    :param inertia_lv:
    :param iterator_factor:
    :param wall_init_thck:
    :param material_type:
    :return:
    """
    plaster_thck = 0.002
    plaster_cond = thermal_mat_obj['plaster'][inertia_lv].thermalcond

    term1 = plaster_thck / plaster_cond

    wall_thck = wall_init_thck - iterator_factor

    wall_cond = thermal_mat_obj[material_type][inertia_lv].thermalcond

    term2 = wall_thck * 0.01 / wall_cond

    R = 2 * (term1 + 2 * term2)

    if wall_thck <= 0:
        raise ValueError("Fictive wall layer has a non-positive thickness.")

    return R, plaster_thck, wall_thck * 0.01, 0.
